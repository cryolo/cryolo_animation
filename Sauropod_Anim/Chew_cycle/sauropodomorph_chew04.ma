//Maya ASCII 2019 scene
//Name: sauropodomorph_chew04.ma
//Last modified: Mon, Dec 23, 2019 01:01:42 PM
//Codeset: 1252
file -rdi 1 -ns "Sauropod_rig1_45" -rfn "Sauropod_rig1_44RN" -op "VERS|2018|UVER|undef|MADE|undef|CHNG|Sat, Dec 07, 2019 12:11:48 AM|ICON|undef|INFO|undef|OBJN|2523|INCL|undef(|LUNI|cm|TUNI|film|AUNI|deg|TDUR|141120000|"
		 -typ "mayaAscii" "E:/Animation Rigs/DinoRigs/Sauropod_rig1.44.ma";
file -r -ns "Sauropod_rig1_45" -dr 1 -rfn "Sauropod_rig1_44RN" -op "VERS|2018|UVER|undef|MADE|undef|CHNG|Sat, Dec 07, 2019 12:11:48 AM|ICON|undef|INFO|undef|OBJN|2523|INCL|undef(|LUNI|cm|TUNI|film|AUNI|deg|TDUR|141120000|"
		 -typ "mayaAscii" "E:/Animation Rigs/DinoRigs/Sauropod_rig1.44.ma";
requires maya "2019";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "3.1.2";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "201812112215-434d8d9c04";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18362)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "02BC8A77-44C2-D688-4AF9-74BC9BF3B893";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -15.714697633716828 4.340488428853357 11.457882863482343 ;
	setAttr ".r" -type "double3" -1.5383527298967612 -53.39999999972099 1.6670276261253793e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "6F5B906D-449D-D784-90DB-D4BD8445C6C9";
	setAttr -k off ".v" no;
	setAttr ".ovr" 1.3;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 19.561007586478791;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 3.4138377843867214 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".dgm" no;
	setAttr ".dr" yes;
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "6A508E05-4BC4-2CCF-52CC-2FBFDE35298D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -7.4129946223680747 1000.1 -6.8866646019173112 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2AD3010F-4564-9C78-DF61-90BACEB1D75E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 42.878582956362351;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "AC32CEF2-4093-896D-520F-A483D9A34BFD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.52853551222283746 6.7833017096403934 1000.1193685427145 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "5B3C839B-4086-0E44-6FD2-28BF10A8C669";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 992.87325778400134;
	setAttr ".ow" 14.778373496015762;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 0.50278137267580225 6.6776127444010953 7.2461107587131988 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "55511BC5-4B0C-1E92-53C1-6EA97F8878B0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 5.7149824480953706 -2.9006493346409927 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "A14DA3F2-4C09-7126-1E99-27B6A2FF0BE6";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 0.17151795204704048;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "left";
	rename -uid "18E77B01-41D5-90C7-8CA8-E7B1172CB3A9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1000.1 1.9181585677749364 0.99744245524296726 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
createNode camera -n "leftShape" -p "left";
	rename -uid "6B66FE04-4D6A-D338-3625-D3B3A785607B";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "left1";
	setAttr ".den" -type "string" "left1_depth";
	setAttr ".man" -type "string" "left1_mask";
	setAttr ".hc" -type "string" "viewSet -ls %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "locator1";
	rename -uid "EE1E349B-4D57-7E2E-1AE4-6E890E675715";
	setAttr ".s" -type "double3" 0.063555206515377508 0.063555206515377508 0.063555206515377508 ;
createNode locator -n "locatorShape1" -p "locator1";
	rename -uid "1FE08869-4568-0F2E-E1B8-1ABB953E0B63";
	setAttr -k off ".v";
createNode parentConstraint -n "locator1_parentConstraint1" -p "locator1";
	rename -uid "CE0B48C7-45FB-70CB-7F3B-BAB83C102A12";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "Lower_Jaw_ControllerW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -0.035608268763526407 -0.35835399479958507 
		1.3510560331575174 ;
	setAttr ".tg[0].tor" -type "double3" 9.4872763272617533 -6.2765698978580735 -4.7178663686354669 ;
	setAttr ".lr" -type "double3" -0.030017819720724925 -0.33751825906668537 0.0037570136919398357 ;
	setAttr ".rst" -type "double3" 0.19366805502074197 6.7038885742985572 7.5202049258224042 ;
	setAttr ".rsrr" -type "double3" 9.9392333795734899e-17 -7.9203265993476247e-16 -4.0378135604517303e-16 ;
	setAttr -k on ".w0";
createNode transform -n "pPlane1";
	rename -uid "6FEA10AB-44E4-A12B-AFB5-02BDA3A94C3A";
	setAttr ".s" -type "double3" 35.770103086177016 35.770103086177016 35.770103086177016 ;
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	rename -uid "0913E8C9-4E35-E743-8134-16BAC50E1F31";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "9955306D-43F8-CDF0-BD84-B8B0ADB1717D";
	setAttr -s 35 ".lnk";
	setAttr -s 35 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "7ADA98BF-4F1D-0F52-4789-F1A8B928AA6A";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "BEF7185E-4978-0543-2525-C7BDDC4A7FAD";
createNode displayLayerManager -n "layerManager";
	rename -uid "18CD26DF-41CE-547A-AD27-1282431B8FFC";
	setAttr ".cdl" 2;
	setAttr -s 4 ".dli[1:3]"  1 0 2;
	setAttr -s 3 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "A650C94E-401D-1E8B-5D81-94ACDF85FBDB";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "4EB38BCE-4F5F-7E75-660A-B19D3B380739";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "9EC4958D-41F7-F361-B95D-8592C988F0AC";
	setAttr ".g" yes;
createNode reference -n "Sauropod_rig1_44RN";
	rename -uid "A4E68035-4086-072C-4E06-4286EEAB93E3";
	setAttr ".fn[0]" -type "string" "E:/Animation Rigs/DinoRigs/Sauropod_rig1.44.mb";
	setAttr -s 330 ".phl";
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".phl[441]" 0;
	setAttr ".phl[442]" 0;
	setAttr ".phl[443]" 0;
	setAttr ".phl[444]" 0;
	setAttr ".phl[445]" 0;
	setAttr ".phl[446]" 0;
	setAttr ".phl[447]" 0;
	setAttr ".phl[448]" 0;
	setAttr ".phl[449]" 0;
	setAttr ".phl[450]" 0;
	setAttr ".phl[451]" 0;
	setAttr ".phl[452]" 0;
	setAttr ".phl[453]" 0;
	setAttr ".phl[454]" 0;
	setAttr ".phl[455]" 0;
	setAttr ".phl[456]" 0;
	setAttr ".phl[457]" 0;
	setAttr ".phl[458]" 0;
	setAttr ".phl[459]" 0;
	setAttr ".phl[460]" 0;
	setAttr ".phl[461]" 0;
	setAttr ".phl[462]" 0;
	setAttr ".phl[463]" 0;
	setAttr ".phl[464]" 0;
	setAttr ".phl[465]" 0;
	setAttr ".phl[466]" 0;
	setAttr ".phl[467]" 0;
	setAttr ".phl[468]" 0;
	setAttr ".phl[469]" 0;
	setAttr ".phl[470]" 0;
	setAttr ".phl[471]" 0;
	setAttr ".phl[472]" 0;
	setAttr ".phl[473]" 0;
	setAttr ".phl[474]" 0;
	setAttr ".phl[475]" 0;
	setAttr ".phl[476]" 0;
	setAttr ".phl[477]" 0;
	setAttr ".phl[478]" 0;
	setAttr ".phl[479]" 0;
	setAttr ".phl[480]" 0;
	setAttr ".phl[481]" 0;
	setAttr ".phl[482]" 0;
	setAttr ".phl[483]" 0;
	setAttr ".phl[484]" 0;
	setAttr ".phl[485]" 0;
	setAttr ".phl[486]" 0;
	setAttr ".phl[487]" 0;
	setAttr ".phl[488]" 0;
	setAttr ".phl[489]" 0;
	setAttr ".phl[490]" 0;
	setAttr ".phl[491]" 0;
	setAttr ".phl[492]" 0;
	setAttr ".phl[493]" 0;
	setAttr ".phl[494]" 0;
	setAttr ".phl[495]" 0;
	setAttr ".phl[496]" 0;
	setAttr ".phl[497]" 0;
	setAttr ".phl[498]" 0;
	setAttr ".phl[499]" 0;
	setAttr ".phl[500]" 0;
	setAttr ".phl[501]" 0;
	setAttr ".phl[502]" 0;
	setAttr ".phl[503]" 0;
	setAttr ".phl[504]" 0;
	setAttr ".phl[505]" 0;
	setAttr ".phl[506]" 0;
	setAttr ".phl[507]" 0;
	setAttr ".phl[508]" 0;
	setAttr ".phl[509]" 0;
	setAttr ".phl[510]" 0;
	setAttr ".phl[511]" 0;
	setAttr ".phl[512]" 0;
	setAttr ".phl[513]" 0;
	setAttr ".phl[514]" 0;
	setAttr ".phl[515]" 0;
	setAttr ".phl[516]" 0;
	setAttr ".phl[517]" 0;
	setAttr ".phl[518]" 0;
	setAttr ".phl[519]" 0;
	setAttr ".phl[520]" 0;
	setAttr ".phl[521]" 0;
	setAttr ".phl[522]" 0;
	setAttr ".phl[523]" 0;
	setAttr ".phl[524]" 0;
	setAttr ".phl[525]" 0;
	setAttr ".phl[526]" 0;
	setAttr ".phl[527]" 0;
	setAttr ".phl[528]" 0;
	setAttr ".phl[529]" 0;
	setAttr ".phl[530]" 0;
	setAttr ".phl[531]" 0;
	setAttr ".phl[532]" 0;
	setAttr ".phl[533]" 0;
	setAttr ".phl[534]" 0;
	setAttr ".phl[535]" 0;
	setAttr ".phl[536]" 0;
	setAttr ".phl[537]" 0;
	setAttr ".phl[538]" 0;
	setAttr ".phl[539]" 0;
	setAttr ".phl[540]" 0;
	setAttr ".phl[541]" 0;
	setAttr ".phl[542]" 0;
	setAttr ".phl[543]" 0;
	setAttr ".phl[544]" 0;
	setAttr ".phl[545]" 0;
	setAttr ".phl[546]" 0;
	setAttr ".phl[547]" 0;
	setAttr ".phl[548]" 0;
	setAttr ".phl[549]" 0;
	setAttr ".phl[550]" 0;
	setAttr ".phl[551]" 0;
	setAttr ".phl[552]" 0;
	setAttr ".phl[553]" 0;
	setAttr ".phl[554]" 0;
	setAttr ".phl[555]" 0;
	setAttr ".phl[556]" 0;
	setAttr ".phl[557]" 0;
	setAttr ".phl[558]" 0;
	setAttr ".phl[559]" 0;
	setAttr ".phl[560]" 0;
	setAttr ".phl[561]" 0;
	setAttr ".phl[562]" 0;
	setAttr ".phl[563]" 0;
	setAttr ".phl[564]" 0;
	setAttr ".phl[565]" 0;
	setAttr ".phl[566]" 0;
	setAttr ".phl[567]" 0;
	setAttr ".phl[568]" 0;
	setAttr ".phl[569]" 0;
	setAttr ".phl[570]" 0;
	setAttr ".phl[571]" 0;
	setAttr ".phl[572]" 0;
	setAttr ".phl[573]" 0;
	setAttr ".phl[574]" 0;
	setAttr ".phl[575]" 0;
	setAttr ".phl[576]" 0;
	setAttr ".phl[577]" 0;
	setAttr ".phl[578]" 0;
	setAttr ".phl[579]" 0;
	setAttr ".phl[580]" 0;
	setAttr ".phl[581]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Sauropod_rig1_44RN"
		"Sauropod_rig1_44RN" 69
		2 "|Sauropod_rig1_45:L_Ebow_Group|Sauropod_rig1_45:L_Arm_Group|Sauropod_rig1_45:L_arm_IK" 
		"translate" " -type \"double3\" 0.89709581265462701 0.32456883227442757 1.94535873176593421"
		
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base" 
		"rotate" " -type \"double3\" -12.73113428723788587 3.12571419273161144 -0.66134601122473036"
		
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base" 
		"rotatePivot" " -type \"double3\" 0 5.4513859748840332 -4.40799236297607422"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base" 
		"scalePivot" " -type \"double3\" 0 5.4513859748840332 -4.40799236297607422"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base" 
		"Tail_Curl_Side" " -av -k 1 0"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base" 
		"Tail_Curl_Up" " -av -k 1 0"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid" 
		"rotate" " -type \"double3\" 0 0.59195924134750633 0"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid" 
		"rotatePivot" " -type \"double3\" 0 5.18900585174560547 -5.89481210708618164"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid" 
		"scalePivot" " -type \"double3\" 0 5.18900585174560547 -5.89481210708618164"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1" 
		"rotate" " -type \"double3\" -3.25865702416931358 -2.94331940523469493 0.16754091159466306"
		
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1" 
		"rotatePivot" " -type \"double3\" 0 4.93634366989135742 -7.82864904403686523"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1" 
		"scalePivot" " -type \"double3\" 0 4.93634366989135742 -7.82864904403686523"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2" 
		"rotate" " -type \"double3\" 3.50238485726959814 -2.63572926952508135 -0.16128868746731237"
		
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2" 
		"rotatePivot" " -type \"double3\" 0 4.73227071762084961 -9.8284759521484375"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2" 
		"scalePivot" " -type \"double3\" 0 4.73227071762084961 -9.8284759521484375"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid" 
		"rotate" " -type \"double3\" 5.1957870232099399 -4.32119930357451221 -0.13963312711033846"
		
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid" 
		"rotatePivot" " -type \"double3\" 0 4.58650398254394531 -11.77116966247558594"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid" 
		"scalePivot" " -type \"double3\" 0 4.58650398254394531 -11.77116966247558594"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip" 
		"rotate" " -type \"double3\" 5.4531486185766056 -0.2047644639018775 -0.21193386903643077"
		
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip" 
		"rotatePivot" " -type \"double3\" 0 4.43101930618286133 -13.68942546844482422"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip" 
		"scalePivot" " -type \"double3\" 0 4.43101930618286133 -13.68942546844482422"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"Both_Eyelids" " -av -k 1 0"
		2 "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"Both_Eyelids" " -av -k 1 0"
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base.Tail_Curl_Side" 
		"Sauropod_rig1_44RN.placeHolderList[38]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base.Tail_Curl_Up" 
		"Sauropod_rig1_44RN.placeHolderList[39]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[40]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[41]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[42]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[43]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[44]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[45]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[46]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[47]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[48]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[49]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[50]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[51]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[52]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[53]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[54]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[55]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[56]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[57]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[58]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[59]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[60]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[61]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[62]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_Controller_Base|Sauropod_rig1_45:Tail_Controller_LowerMid|Sauropod_rig1_45:Tail_Controller_Mid_1|Sauropod_rig1_45:Tail_Controller_Mid_2|Sauropod_rig1_45:Tail_Controller_Upper_Mid|Sauropod_rig1_45:Tail_Controller_Tip.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[63]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.Both_Eyelids" 
		"Sauropod_rig1_44RN.placeHolderList[268]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:locator1|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.Both_Eyelids" 
		"Sauropod_rig1_44RN.placeHolderList[279]" ""
		"Sauropod_rig1_44RN" 723
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Mesh|Sauropod_rig1_45:L_Eye_grp|Sauropod_rig1_45:L_Top_Eyelid|Sauropod_rig1_45:L_Top_EyelidShape" 
		"uvSet[0].uvSetName" " -type \"string\" \"map1\""
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:All_IKHandle_GRP|Sauropod_rig1_45:L_Foot_Group|Sauropod_rig1_45:Foot_Roll|Sauropod_rig1_45:Left_Heel_IK" 
		"translate" " -type \"double3\" 1.01893693370528027 0.7115038903324602 -2.59199081229677297"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:All_IKHandle_GRP|Sauropod_rig1_45:R_Elbow_Group|Sauropod_rig1_45:R_Arm_Group|Sauropod_rig1_45:R_Arm_IK" 
		"translate" " -type \"double3\" -0.897096 0.324569000000001 1.94536000000000042"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"translate" " -type \"double3\" -0.33373086870771229 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"rotate" " -type \"double3\" 0 -15.45837283825547459 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"rotatePivot" " -type \"double3\" -1.00326001644134521 0.20864272117614746 -2.04487919807434082"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"scalePivot" " -type \"double3\" -1.00326001644134521 0.20864272117614746 -2.04487919807434082"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"R_Foot_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"R_Foot_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"R_Foot_Roll" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"R_Left_Toe_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"R_Right_Toe_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"R_Mid_Toe_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"R_Left_Toe_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller" 
		"R_Right_Toe_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller" 
		"translate" " -type \"double3\" 0.3105045171968861 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller" 
		"rotatePivot" " -type \"double3\" 1.07419717311859131 1.24358928203582764 -2.97947931289672852"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller" 
		"scalePivot" " -type \"double3\" 1.07419717311859131 1.24358928203582764 -2.97947931289672852"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"translate" " -type \"double3\" 0 0 -0.32144552295996442"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"rotatePivot" " -type \"double3\" 1.00325620174407959 0.20864272117614746 -2.04487919807434082"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"scalePivot" " -type \"double3\" 1.00325620174407959 0.20864272117614746 -2.04487919807434082"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"L_Foot_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"L_Foot_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"L_Foot_Roll" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"L_Left_Toe_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"L_Right_Toe_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"L_Mid_Toe_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"L_Left_Toe_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller" 
		"L_Right_Toe_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"translate" " -type \"double3\" -0.048655931166913191 -0.050170585782662105 0.00053914607558260171"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"rotate" " -type \"double3\" 0 -3.00000000000000044 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"rotatePivot" " -type \"double3\" 0 5.76655197143554688 -2.93520951271057129"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller" 
		"scalePivot" " -type \"double3\" 0 5.76655197143554688 -2.93520951271057129"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller" 
		"rotate" " -type \"double3\" 0 -2.21712186658603061 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller" 
		"rotatePivot" " -type \"double3\" 0 5.76655197143554688 -2.93520951271057129"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller" 
		"scalePivot" " -type \"double3\" 0 5.76655197143554688 -2.93520951271057129"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"rotatePivot" " -type \"double3\" 0 5.61759281158447266 -0.73392307758331299"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller" 
		"scalePivot" " -type \"double3\" 0 5.61759281158447266 -0.73392307758331299"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"rotatePivot" " -type \"double3\" 0 4.11144924163818359 2.07975125312805176"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint" 
		"scalePivot" " -type \"double3\" 0 4.11144924163818359 2.07975125312805176"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"rotate" " -type \"double3\" -9.7308005038502543 8.78659085633594117 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"rotatePivot" " -type \"double3\" 0 3.37390422821044922 3.09305691719055176"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller" 
		"scalePivot" " -type \"double3\" 0 3.37390422821044922 3.09305691719055176"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"rotate" " -type \"double3\" 6.10378063603428167 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"rotatePivot" " -type \"double3\" 0 4.58940696716308594 4.93941259384155273"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller" 
		"scalePivot" " -type \"double3\" 0 4.58940696716308594 4.93941259384155273"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"rotate" " -type \"double3\" 3.47259137830300624 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"rotatePivot" " -type \"double3\" 0 5.74797868728637695 5.94902515411376953"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller" 
		"scalePivot" " -type \"double3\" 0 5.74797868728637695 5.94902515411376953"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"translate" " -type \"double3\" 0 -6.9194485082608916e-08 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"rotate" " -type \"double3\" 6.07486915696001795 1.49118583508483349 4.80620293196216331"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"rotatePivot" " -type \"double3\" 0 6.88649559020996094 6.59818124771118164"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller" 
		"scalePivot" " -type \"double3\" 0 6.88649559020996094 6.59818124771118164"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"translate" " -type \"double3\" 0.0045355615023148898 0.053422209827814052 -0.0060459244415562406"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"translate" " -type \"double3\" 0.002855679121628589 0.033822829697805853 -0.0032562574425446522"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"rotate" " -type \"double3\" -1.81742865949626697 3.27275444436320173 0.42654531688515179"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"rotatePivot" " -type \"double3\" 0 6.24759912490844727 6.85721731185913086"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"scalePivot" " -type \"double3\" 0 6.24759912490844727 6.85721731185913086"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"Tongue_Curl_Up" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller" 
		"Tongue_Curl_Tip" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"translate" " -type \"double3\" 0.0029266464995921938 0.037038232721325316 -0.0060432306282166871"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"rotatePivot" " -type \"double3\" 0.32621374726295471 5.98568344116210938 7.99870109558105469"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller" 
		"scalePivot" " -type \"double3\" 0.32621374726295471 5.98568344116210938 7.99870109558105469"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"translate" " -type \"double3\" 0.0029266464995921938 0.037038232721325316 -0.0060432306282166871"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"scaleX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"scaleY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller" 
		"scaleZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"rotatePivot" " -type \"double3\" 1.28754496574401855 6.91144990921020508 7.39737844467163086"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"scalePivot" " -type \"double3\" 1.28754496574401855 6.91144990921020508 7.39737844467163086"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"Top_Eyelid" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller" 
		"Bottom_Eyelid" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"visibility" " -av 1"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"Top_Eyelid" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller" 
		"Bottom_Eyelid" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL" 
		"rotate" " -type \"double3\" -10.1793992579014585 0.55733658003806963 0.52572076659562694"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL" 
		"rotate" " -type \"double3\" -2.87882648628588989 -0.4241590659578412 0.04213488044002936"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL" 
		"rotate" " -type \"double3\" -4.82934393428914355 -5.32465187325838585 0.46839166058277226"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL" 
		"rotate" " -type \"double3\" -1.0676511849349164 -3.09478284725814534 -0.60696103526737255"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL" 
		"rotate" " -type \"double3\" 10.00875206696955289 -2.97304207645729379 -0.56894448457528435"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL|Sauropod_rig1_45:Tail_6_Driver_GRP|Sauropod_rig1_45:Tail_6_CNTRL" 
		"rotate" " -type \"double3\" 12.22693007011706356 -6.55392461841495511 -1.50262632524641204"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL|Sauropod_rig1_45:Tail_6_Driver_GRP|Sauropod_rig1_45:Tail_6_CNTRL" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL|Sauropod_rig1_45:Tail_6_Driver_GRP|Sauropod_rig1_45:Tail_6_CNTRL" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL|Sauropod_rig1_45:Tail_6_Driver_GRP|Sauropod_rig1_45:Tail_6_CNTRL" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"translate" " -type \"double3\" 0.25 0.0064543845230639052 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"rotatePivot" " -type \"double3\" 0.7863813042640686 3.45776724815368652 2.36232113838195801"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller" 
		"scalePivot" " -type \"double3\" 0.7863813042640686 3.45776724815368652 2.36232113838195801"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"translate" " -type \"double3\" 0 -0.011856752692324002 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"rotatePivot" " -type \"double3\" 0.89709579944610596 0.32456883788108826 1.9453587532043457"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"scalePivot" " -type \"double3\" 0.89709579944610596 0.32456883788108826 1.9453587532043457"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Hand_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Hand_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Thumb_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Index_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Middle_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Ring_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Pinky_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Thumb_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Index_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Ring_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller" 
		"L_Pinky_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"translate" " -type \"double3\" 0 -0.0040172269881030981 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"rotatePivot" " -type \"double3\" -0.78638100624084473 3.45776724815368652 2.36232113838195801"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller" 
		"scalePivot" " -type \"double3\" -0.78638100624084473 3.45776724815368652 2.36232113838195801"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"translate" " -type \"double3\" -0.083741507737011445 0.022066587764167575 0.0023460326947661691"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"translateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"translateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"translateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"rotate" " -type \"double3\" 0 -13.9818371465004514 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"rotateX" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"rotateY" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"rotateZ" " -av"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"rotatePivot" " -type \"double3\" -0.89709597826004028 0.32456883788108826 1.9453587532043457"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"scalePivot" " -type \"double3\" -0.89709597826004028 0.32456883788108826 1.9453587532043457"
		
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Hand_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Hand_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Thumb_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Index_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Middle_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Ring_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Pinky_Curl" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Thumb_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Index_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Ring_Spread" " -av -k 1 0"
		2 "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller" 
		"R_Pinky_Spread" " -av -k 1 0"
		3 "Sauropod_rig1_45:Left_Leg.drawInfo" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.drawOverride" 
		""
		3 "Sauropod_rig1_45:Right_Leg.drawInfo" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.drawOverride" 
		""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.R_Foot_Spread" 
		"Sauropod_rig1_44RN.placeHolderList[280]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.R_Foot_Curl" 
		"Sauropod_rig1_44RN.placeHolderList[281]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.R_Foot_Roll" 
		"Sauropod_rig1_44RN.placeHolderList[282]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.R_Left_Toe_Curl" 
		"Sauropod_rig1_44RN.placeHolderList[283]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.R_Right_Toe_Curl" 
		"Sauropod_rig1_44RN.placeHolderList[284]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.R_Mid_Toe_Curl" 
		"Sauropod_rig1_44RN.placeHolderList[285]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.R_Left_Toe_Spread" 
		"Sauropod_rig1_44RN.placeHolderList[286]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.R_Right_Toe_Spread" 
		"Sauropod_rig1_44RN.placeHolderList[287]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[288]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[289]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[290]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[291]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[292]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[293]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Right_Leg_Controller|Sauropod_rig1_45:R_Foot_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[294]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.L_Foot_Spread" 
		"Sauropod_rig1_44RN.placeHolderList[295]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.L_Foot_Curl" 
		"Sauropod_rig1_44RN.placeHolderList[296]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.L_Foot_Roll" 
		"Sauropod_rig1_44RN.placeHolderList[297]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.L_Left_Toe_Curl" 
		"Sauropod_rig1_44RN.placeHolderList[298]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.L_Right_Toe_Curl" 
		"Sauropod_rig1_44RN.placeHolderList[299]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.L_Mid_Toe_Curl" 
		"Sauropod_rig1_44RN.placeHolderList[300]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.L_Left_Toe_Spread" 
		"Sauropod_rig1_44RN.placeHolderList[301]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.L_Right_Toe_Spread" 
		"Sauropod_rig1_44RN.placeHolderList[302]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[303]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[304]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[305]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[306]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[307]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[308]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Left_Leg_Controller|Sauropod_rig1_45:L_Foot_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[309]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[310]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[311]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[312]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[313]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[314]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[315]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[316]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[317]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[318]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[319]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Hip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[320]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[321]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[322]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[323]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[324]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[325]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[326]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[327]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[328]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[329]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[330]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[331]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[332]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[333]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[334]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[335]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[336]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[337]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[338]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[339]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[340]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[341]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[342]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[343]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[344]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[345]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[346]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[347]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[348]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[349]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[350]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[351]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[352]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[353]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[354]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[355]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[356]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[357]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[358]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[359]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[360]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[361]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[362]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[363]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[364]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[365]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[366]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[367]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[368]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[369]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[370]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[371]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[372]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[373]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[374]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[375]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[376]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[377]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[378]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[379]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Top_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[380]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[381]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[382]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[383]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[384]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[385]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[386]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[387]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[388]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[389]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[390]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Mid_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[391]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[392]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[393]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[394]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[395]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[396]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[397]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[398]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[399]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[400]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[401]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Upper_Bottom_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[402]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[403]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[404]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[405]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[406]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[407]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[408]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[409]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[410]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[411]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[412]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Top_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[413]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[414]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[415]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[416]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[417]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[418]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[419]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[420]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[421]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[422]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[423]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Mid_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[424]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[425]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[426]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[427]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[428]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[429]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[430]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[431]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[432]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[433]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[434]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Upper_Bottom_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[435]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.Tongue_Curl_Up" 
		"Sauropod_rig1_44RN.placeHolderList[436]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.Tongue_Curl_Tip" 
		"Sauropod_rig1_44RN.placeHolderList[437]" ""
		5 3 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.translate" 
		"Sauropod_rig1_44RN.placeHolderList[438]" ""
		5 3 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.rotatePivot" 
		"Sauropod_rig1_44RN.placeHolderList[439]" ""
		5 3 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.rotatePivotTranslate" 
		"Sauropod_rig1_44RN.placeHolderList[440]" ""
		5 3 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.rotate" 
		"Sauropod_rig1_44RN.placeHolderList[441]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[442]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[443]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[444]" ""
		5 3 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.rotateOrder" 
		"Sauropod_rig1_44RN.placeHolderList[445]" ""
		5 3 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.scale" 
		"Sauropod_rig1_44RN.placeHolderList[446]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[447]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[448]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[449]" ""
		5 3 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.parentMatrix" 
		"Sauropod_rig1_44RN.placeHolderList[450]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[451]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[452]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[453]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[454]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[455]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[456]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[457]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[458]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[459]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[460]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[461]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[462]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Top_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[463]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[464]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[465]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[466]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[467]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[468]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[469]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[470]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[471]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[472]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[473]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Mid_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[474]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[475]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[476]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[477]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[478]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[479]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[480]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[481]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[482]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[483]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[484]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:L_Lower_Bottom_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[485]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[486]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[487]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[488]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[489]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[490]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[491]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[492]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[493]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[494]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[495]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Bottom_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[496]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[497]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[498]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[499]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[500]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[501]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[502]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[503]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[504]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[505]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[506]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Mid_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[507]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[508]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[509]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[510]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[511]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[512]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[513]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.scaleX" 
		"Sauropod_rig1_44RN.placeHolderList[514]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.scaleY" 
		"Sauropod_rig1_44RN.placeHolderList[515]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.scaleZ" 
		"Sauropod_rig1_44RN.placeHolderList[516]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[517]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:Lower_Jaw_Controller|Sauropod_rig1_45:R_Lower_Top_Lip_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[518]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.Top_Eyelid" 
		"Sauropod_rig1_44RN.placeHolderList[519]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.Bottom_Eyelid" 
		"Sauropod_rig1_44RN.placeHolderList[520]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[521]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[522]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[523]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[524]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[525]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[526]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[527]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:L_Eyeball_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[528]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.Top_Eyelid" 
		"Sauropod_rig1_44RN.placeHolderList[529]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.Bottom_Eyelid" 
		"Sauropod_rig1_44RN.placeHolderList[530]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[531]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[532]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[533]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.visibility" 
		"Sauropod_rig1_44RN.placeHolderList[534]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[535]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[536]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[537]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Middle_Waist_Controller|Sauropod_rig1_45:Upper_Body_Contraint|Sauropod_rig1_45:Neck_Base_Contoller|Sauropod_rig1_45:Neck_Lower_Mid_Controller|Sauropod_rig1_45:Neck_Upper_Mid_Controller|Sauropod_rig1_45:Head_Base_Controller|Sauropod_rig1_45:Head_Controller|Sauropod_rig1_45:Face_Group|Sauropod_rig1_45:R_Eyeball_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[538]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL.Tail_Curl_Side" 
		"Sauropod_rig1_44RN.placeHolderList[539]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL.Tail_Curl_Up" 
		"Sauropod_rig1_44RN.placeHolderList[540]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[541]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[542]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[543]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[544]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[545]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[546]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[547]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[548]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[549]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[550]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[551]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[552]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[553]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[554]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[555]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL|Sauropod_rig1_45:Tail_6_Driver_GRP|Sauropod_rig1_45:Tail_6_CNTRL.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[556]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL|Sauropod_rig1_45:Tail_6_Driver_GRP|Sauropod_rig1_45:Tail_6_CNTRL.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[557]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:Tail_1_Driver_GRP|Sauropod_rig1_45:Tail_1_CNTRL|Sauropod_rig1_45:Tail_2_Driver_GRP|Sauropod_rig1_45:Tail_2_CNTRL|Sauropod_rig1_45:Tail_3_Driver_GRP|Sauropod_rig1_45:Tail_3_CNTRL|Sauropod_rig1_45:Tail_4_Driver_GRP|Sauropod_rig1_45:Tail_4_CNTRL|Sauropod_rig1_45:Tail_5_Driver_GRP|Sauropod_rig1_45:Tail_5_CNTRL|Sauropod_rig1_45:Tail_6_Driver_GRP|Sauropod_rig1_45:Tail_6_CNTRL.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[558]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[559]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[560]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[561]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[562]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[563]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[564]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[565]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[566]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[567]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[568]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:L_Shoulder_Controller|Sauropod_rig1_45:L_Elbow_Controller|Sauropod_rig1_45:L_Hand_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[569]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller.translateX" 
		"Sauropod_rig1_44RN.placeHolderList[570]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller.translateY" 
		"Sauropod_rig1_44RN.placeHolderList[571]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller.translateZ" 
		"Sauropod_rig1_44RN.placeHolderList[572]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[573]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[574]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[575]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[576]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller.rotateX" 
		"Sauropod_rig1_44RN.placeHolderList[577]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller.rotateY" 
		"Sauropod_rig1_44RN.placeHolderList[578]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller.rotateZ" 
		"Sauropod_rig1_44RN.placeHolderList[579]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[580]" ""
		5 4 "Sauropod_rig1_44RN" "|Sauropod_rig1_45:Sauropod_Character|Sauropod_rig1_45:Sauropod_Rig_GRP|Sauropod_rig1_45:Rig_LOC|Sauropod_rig1_45:Main_Controller_GRP|Sauropod_rig1_45:Main_Controller|Sauropod_rig1_45:Main_Body_Cog_Controller|Sauropod_rig1_45:R_Shoulder_Contoller|Sauropod_rig1_45:R_Elbow_Controller|Sauropod_rig1_45:R_Hand_Controller.drawOverride" 
		"Sauropod_rig1_44RN.placeHolderList[581]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode displayLayer -n "Controls";
	rename -uid "BF94CECC-47D4-6361-106A-2AA81686643C";
	setAttr ".do" 1;
createNode animCurveTA -n "L_Foot_Controller_rotateX";
	rename -uid "68917825-40B3-1FB3-399D-2494D1C2C317";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Foot_Controller_rotateY";
	rename -uid "3760A048-4E64-15CC-8E12-7F8660F9A85C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Foot_Controller_rotateZ";
	rename -uid "FC6746D2-4769-8DDB-EC3A-0F866EAAC890";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Hip_Controller_rotateX";
	rename -uid "F2C7478C-44FD-08EB-86BF-C7B919DB1B2C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 50 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Hip_Controller_rotateY";
	rename -uid "8F058EF9-4769-72F8-0CD5-86A4738E6B48";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 6.0423879387953203 50 -2.3762654646554959
		 100 6.0423879387953203;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Hip_Controller_rotateZ";
	rename -uid "0B4429FD-4F99-9D52-2D21-F68265A7E42B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 50 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Middle_Waist_Controller_rotateX";
	rename -uid "D6C22891-4312-54EB-D150-B8ADFC25FCA0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Middle_Waist_Controller_rotateY";
	rename -uid "A1D2F62E-478E-1FDE-8E7D-66A68CC8A2CC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Middle_Waist_Controller_rotateZ";
	rename -uid "E387932E-443E-7C18-3EF9-369383149261";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Upper_Body_Contraint_rotateX";
	rename -uid "15132E42-4364-FAF2-F561-8C97A8107512";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Upper_Body_Contraint_rotateY";
	rename -uid "D6999740-4A65-B950-A15D-4B944A8B3636";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Upper_Body_Contraint_rotateZ";
	rename -uid "0C991BFB-4650-5994-3D3F-0FA26856524F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Shoulder_Controller_rotateX";
	rename -uid "834EC507-4D7C-E49F-E77F-5FBEA6EDE0E1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "L_Shoulder_Controller_rotateY";
	rename -uid "2954821C-4BBF-5785-AEE9-C5800634B4C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "L_Shoulder_Controller_rotateZ";
	rename -uid "54A94938-4A5D-995A-9247-068EA8B8E4B8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "L_Elbow_Controller_rotateX";
	rename -uid "E1E24A85-45C9-81BE-70DA-23BB58FD9CD7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Elbow_Controller_rotateY";
	rename -uid "97BB25B7-4955-F7F2-F6B9-BEACE84E4293";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Elbow_Controller_rotateZ";
	rename -uid "C7DFC6D7-47C7-77F8-3FD0-459BE31572C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Neck_Base_Contoller_rotateX";
	rename -uid "9C723DF2-43F6-057D-7B62-A3AEB89DB289";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -14.999999999999998 54 -9.2874984499181981
		 69 -11.657130065025271 100 -14.999999999999998;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99889488009226368 1 0.99789268352013039 
		0.9988946480519435;
	setAttr -s 4 ".kiy[0:3]"  -0.047000197078972621 0 -0.064885993688878596 
		-0.047005128371105154;
	setAttr -s 4 ".kox[0:3]"  0.99889488554297223 1 0.99789268352013027 
		0.99889464866146738;
	setAttr -s 4 ".koy[0:3]"  -0.047000081234956796 0 -0.064885993688878596 
		-0.047005115418258893;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Neck_Base_Contoller_rotateY";
	rename -uid "48027CED-4946-5F31-3224-98AD07B8EEA4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 10 33 7.9088954277953807 78 12.250679040289736
		 100 10;
	setAttr -s 4 ".kix[0:3]"  0.99472607605168895 1.033333333333333 0.79999999999999982 
		0.99472497617487132;
	setAttr -s 4 ".kiy[0:3]"  -0.10256721514601921 0 0 -0.1025778815042588;
	setAttr -s 4 ".kox[0:3]"  0.99472607381356482 0.73333333333333339 
		0.73333333333333339 0.99472497337815879;
	setAttr -s 4 ".koy[0:3]"  -0.10256723685198184 0 0 -0.10257790862471915;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Neck_Base_Contoller_rotateZ";
	rename -uid "1580E071-4286-1016-D281-EBBA7AE6FBF8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 54 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Neck_Lower_Mid_Controller_rotateX";
	rename -uid "879BC111-498A-8908-11BA-33A53DDF4BBB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Neck_Lower_Mid_Controller_rotateY";
	rename -uid "143BDC06-4E19-746A-E161-E687F059E80F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Neck_Lower_Mid_Controller_rotateZ";
	rename -uid "AA12B90B-46D1-E653-A13B-8DBD5B862928";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Neck_Upper_Mid_Controller_rotateX";
	rename -uid "A6EF6C7F-4786-916E-991A-B09712AE488A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 6.1037806360342817 25 6.1037806360342817
		 50 6.1037806360342817 100 6.1037806360342817;
createNode animCurveTA -n "Neck_Upper_Mid_Controller_rotateY";
	rename -uid "702C8A80-4E30-E75E-6E21-5D82AE4AFE0A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Neck_Upper_Mid_Controller_rotateZ";
	rename -uid "13F32F3D-4C7B-E73C-F8B7-2D945661DC53";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Head_Base_Controller_rotateX";
	rename -uid "E49DBEAE-4644-1A0C-1697-04AC580C1AA6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.4725913783030062 25 3.4725913783030062
		 50 3.4725913783030062 100 3.4725913783030062;
createNode animCurveTA -n "Head_Base_Controller_rotateY";
	rename -uid "4E358832-4F62-A042-CE41-42A189C20D6D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Head_Base_Controller_rotateZ";
	rename -uid "B52E242C-420B-3D7B-54E1-359D6E7A1212";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Head_Controller_rotateX";
	rename -uid "7C25F003-4B2A-A583-9BC7-CC92ACF9AEF3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 5.9617562593140185 29 6.0757492042704211
		 47 6.0747045399280086 100 5.9617562593140185;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Head_Controller_rotateY";
	rename -uid "2D6B40E8-4297-19BE-46F3-339DAFBC84CE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -0.37665930981095963 29 0.39920394523428321
		 47 1.4978049718099411 100 -0.37665930981095963;
	setAttr -s 4 ".kit[1:3]"  1 18 1;
	setAttr -s 4 ".kot[1:3]"  1 18 1;
	setAttr -s 4 ".kix[1:3]"  0.9994980186666631 1 1;
	setAttr -s 4 ".kiy[1:3]"  0.031681393299767664 0 0;
	setAttr -s 4 ".kox[1:3]"  0.99949801835255014 1 1;
	setAttr -s 4 ".koy[1:3]"  0.031681403209540333 0 0;
createNode animCurveTA -n "Head_Controller_rotateZ";
	rename -uid "7BA9923E-45C4-0C5C-902A-E19EC5E9AE88";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.6020520415224797 29 4.8155715342569012
		 47 4.8044504903768059 100 3.6020520415224797;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "L_Eyeball_Controller_rotateX";
	rename -uid "9920B556-4AD9-EF3D-03B2-05950E0C3FC8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTA -n "L_Eyeball_Controller_rotateY";
	rename -uid "D6118D1B-44B8-8D87-6CD9-21A15F32000A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTA -n "L_Eyeball_Controller_rotateZ";
	rename -uid "60125520-4A39-3150-9175-FAAF2DB0DF01";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTA -n "L_Upper_Bottom_Lip_Controller_rotateX";
	rename -uid "5430728B-4804-FA7A-C7D5-D783C2400D6B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 19 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Upper_Bottom_Lip_Controller_rotateY";
	rename -uid "E6B6CB0A-4C3F-0168-8D0C-D8B1FB896172";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 19 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Upper_Bottom_Lip_Controller_rotateZ";
	rename -uid "025CA158-4916-98CC-EC69-EEA90086FCFC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 19 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Upper_Mid_Lip_Controller_rotateX";
	rename -uid "19E6C160-4D61-B644-6677-3F9DEBDB6419";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Upper_Mid_Lip_Controller_rotateY";
	rename -uid "22EF2880-4FD0-6C05-B661-8896078D3330";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Upper_Mid_Lip_Controller_rotateZ";
	rename -uid "08F827E7-424F-DF17-8622-0F930531C68B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Upper_Top_Lip_Controller_rotateX";
	rename -uid "6B688031-4BB7-D653-5E51-C9BD35AECCC7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Upper_Top_Lip_Controller_rotateY";
	rename -uid "1972BB93-441E-4633-2521-77BBD6836D87";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Upper_Top_Lip_Controller_rotateZ";
	rename -uid "690E137D-436F-60B7-9E20-F4896C2CE390";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Lower_Bottom_Lip_Controller_rotateX";
	rename -uid "7CAD4733-45C6-70D0-80D3-2F93444CF3B5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 60 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "L_Lower_Bottom_Lip_Controller_rotateY";
	rename -uid "3606C2FB-45DE-2F76-9AC8-C3BBB1C055DA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 60 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "L_Lower_Bottom_Lip_Controller_rotateZ";
	rename -uid "56D7B119-46A8-59BB-9232-43824E38F7E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 60 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "L_Lower_Mid_Lip_Controller_rotateX";
	rename -uid "8EB2B084-4997-4DF3-4855-FA9B33FE1FC4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Lower_Mid_Lip_Controller_rotateY";
	rename -uid "8DBF0DA3-4AFD-29DE-683F-F380A0CCDB05";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Lower_Mid_Lip_Controller_rotateZ";
	rename -uid "6710800C-491D-237F-43A9-94A5815EB0A7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Lower_Top_Lip_Controller_rotateX";
	rename -uid "AF965E80-461E-8279-FC8B-89B3B2784E45";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Lower_Top_Lip_Controller_rotateY";
	rename -uid "1620ACF1-4EC9-0095-596A-1486FB9AE7E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "L_Lower_Top_Lip_Controller_rotateZ";
	rename -uid "24E399DE-4BA5-3376-EB72-16B6A478E560";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Lower_Bottom_Lip_Controller_rotateX";
	rename -uid "FF795C87-4E8A-F8E5-8A4F-5298EBAA3E66";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 60 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "R_Lower_Bottom_Lip_Controller_rotateY";
	rename -uid "8F20D2F4-4FB0-91E1-BEA4-C5AB73B43F2F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 60 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "R_Lower_Bottom_Lip_Controller_rotateZ";
	rename -uid "528286CA-4C80-2A49-CA29-B9890BC8CF4B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 60 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTA -n "R_Lower_Mid_Lip_Controller_rotateX";
	rename -uid "C3E32C74-4144-B15C-FA3F-09A547EEE833";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Lower_Mid_Lip_Controller_rotateY";
	rename -uid "066E66CF-4CED-4E9E-2735-90BEF5F2548E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Lower_Mid_Lip_Controller_rotateZ";
	rename -uid "676EC6E9-4B05-B1C2-9F5E-7AB3E01633E9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Lower_Top_Lip_Controller_rotateX";
	rename -uid "23DEDDB0-4896-BBA0-2609-1091C9F7B1C3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Lower_Top_Lip_Controller_rotateY";
	rename -uid "190D0C2B-4605-E477-47C2-FAA20B4914D6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Lower_Top_Lip_Controller_rotateZ";
	rename -uid "6D42025F-417D-410B-5888-BD9D3A8FBB74";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Eyeball_Controller_rotateX";
	rename -uid "44E0EEFB-41BB-AABC-9117-24B2817DEC09";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTA -n "R_Eyeball_Controller_rotateY";
	rename -uid "6CA27D58-4F29-7990-ADD4-FE9CE77D53C5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTA -n "R_Eyeball_Controller_rotateZ";
	rename -uid "22E50B60-498C-5CB0-2E60-5BB509E937BE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTA -n "R_Upper_Bottom_Lip_Controller_rotateX";
	rename -uid "071A0EB4-448A-B98B-A222-70831B8705F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 26 0 50 0 100 0;
createNode animCurveTA -n "R_Upper_Bottom_Lip_Controller_rotateY";
	rename -uid "6DBBB58B-434E-EE51-BDB4-F996F3C25EC5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 26 0 50 0 100 0;
createNode animCurveTA -n "R_Upper_Bottom_Lip_Controller_rotateZ";
	rename -uid "C6DD7218-41E7-F435-76B2-D38BBEFA0354";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 26 0 50 0 100 0;
createNode animCurveTA -n "R_Upper_Mid_Lip_Controller_rotateX";
	rename -uid "13F56277-4EB1-E420-974A-C897EAF99FE2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Upper_Mid_Lip_Controller_rotateY";
	rename -uid "0CF8E906-4F9D-CCBE-548C-B69FC15BA1FC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Upper_Mid_Lip_Controller_rotateZ";
	rename -uid "B65CA08A-418F-C758-4FDE-1F9023FF1927";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Upper_Top_Lip_Controller_rotateX";
	rename -uid "07708557-4568-80CD-BEB9-0981BCEB0F23";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Upper_Top_Lip_Controller_rotateY";
	rename -uid "BCE1C6F7-49E6-4C64-7844-048A3D2CA3C9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Upper_Top_Lip_Controller_rotateZ";
	rename -uid "FBF73295-49B7-F8B3-BDE1-B3B53AB53010";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Shoulder_Contoller_rotateX";
	rename -uid "D47C164B-428B-EB50-42DB-A0A1C069C99E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kot[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "R_Shoulder_Contoller_rotateY";
	rename -uid "B8AE79CE-4815-71F8-F70E-EC8561E605EF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kot[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "R_Shoulder_Contoller_rotateZ";
	rename -uid "A1D1F78F-4537-3AF1-14B8-E8884D158E2C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kot[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "R_Elbow_Controller_rotateX";
	rename -uid "57EA942F-4C74-53CA-2299-66A58FDAF9F0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Elbow_Controller_rotateY";
	rename -uid "6D82D84A-4836-0E1D-AE0D-CFB9DED2D99C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Elbow_Controller_rotateZ";
	rename -uid "D8F39997-428E-64C8-C81B-6BAC9477B791";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Tail_Controller_Base_rotateX";
	rename -uid "5CEF0B5A-4E6E-53D7-A5CF-87B1835EEC11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -11.924612756490088 25 -13.151601190177249
		 50 -11.957092943190419 75 -13.045566954048319 100 -11.924612756490088;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Tail_Controller_Base_rotateY";
	rename -uid "1A74A5D8-489C-C113-0603-1A8B3B465138";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.6036013535141469 50 4.4889837796410728
		 100 -1.6036013535141469;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Tail_Controller_Base_rotateZ";
	rename -uid "0C15EE85-46F3-C16D-A8CF-86839BB355BF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.33860317663548545 50 -0.94959074831925305
		 100 0.33860317663548545;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Tail_Controller_LowerMid_rotateX";
	rename -uid "78F2BEB8-4632-DDD0-7363-9CB1B91B7EBE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Tail_Controller_LowerMid_rotateY";
	rename -uid "E3D00110-4A9A-7337-ED4E-02B2974138F0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -0.90486212179784231 25 0 50 1.2350584798328652
		 100 -0.90486212179784231;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_Controller_LowerMid_rotateZ";
	rename -uid "4D5CFBCC-4EC6-7A10-9CED-AD8957CE8A16";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Tail_Controller_Mid_1_rotateX";
	rename -uid "1823C32A-40FB-4896-1A32-5EB8C7F23D05";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -3.2533252581203196 29 -3.259837083913284
		 50 -3.2538847237341373 72 -3.2616740809958857 100 -3.2533252581203196;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Tail_Controller_Mid_1_rotateY";
	rename -uid "176C2BE0-493B-4366-115C-9FAF78426251";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -1.0893224001325619 29 -3.7678800758506128
		 50 1.5212438284732623 72 4.0986746035037669 100 -1.0893224001325619;
	setAttr -s 5 ".kit[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kot[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kix[0:4]"  0.99379654295646402 1 0.9954435845578492 
		1 0.99384885090792519;
	setAttr -s 5 ".kiy[0:4]"  -0.11121344886200168 0 0.095352346392839724 
		0 -0.11074502945503517;
	setAttr -s 5 ".kox[0:4]"  0.99379654183874844 1 0.99544358455784909 
		1 0.99384885297416681;
	setAttr -s 5 ".koy[0:4]"  -0.1112134588498384 0 0.09535234639283971 
		0 -0.11074501091215344;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_Controller_Mid_1_rotateZ";
	rename -uid "9DD826D8-4D11-D99D-1DAE-138FFD1B9C08";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0.061915878978415433 29 0.21444638886186193
		 50 -0.086475733308840882 72 -0.23338075640770686 100 0.061915878978415433;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Tail_Controller_Mid_2_rotateX";
	rename -uid "EB23CAFD-4800-DF6A-2E10-F8BDD1E43757";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 3.4994799279937325 29 3.5034280137601432
		 50 3.4981662096975392 72 3.5050666185824597 100 3.4994799279937325;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Tail_Controller_Mid_2_rotateY";
	rename -uid "526C9FEA-4326-CD89-1467-BBA81C3966EA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -1.8551100086932204 29 -3.2925843227978544
		 50 0.98986452504004108 72 3.4548567494828228 100 -1.8551100086932204;
	setAttr -s 5 ".kit[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kot[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kix[0:4]"  0.99165404415108649 1 0.99664172295605336 
		1 0.99155113885912072;
	setAttr -s 5 ".kiy[0:4]"  -0.12892733115517063 0 0.081885750061834281 
		0 -0.12971637917850207;
	setAttr -s 5 ".kox[0:4]"  0.99165404987294592 1 0.99664172295605336 
		1 0.99155114086836005;
	setAttr -s 5 ".koy[0:4]"  -0.1289272871450608 0 0.081885750061834295 
		0 -0.12971636381988849;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_Controller_Mid_2_rotateZ";
	rename -uid "E7D464DA-4B71-CAF1-17FA-FEAA76DF067A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.11342651852458277 29 -0.20146606040055434
		 50 0.060507875603486333 72 0.21147611932189556 100 -0.11342651852458277;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Tail_Controller_Upper_Mid_rotateX";
	rename -uid "AE7651BF-4777-DA0E-FB85-ACBB1EA7C009";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 5.192837548896164 29 5.1959691612385122
		 50 5.1950504355943865 72 5.2151732925485375 100 5.192837548896164;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Tail_Controller_Upper_Mid_rotateY";
	rename -uid "A8AFD7D3-4B48-6B78-8F6B-839FD7B92E6C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -0.82816489384340308 29 -4.855633885995136
		 72 5.2325167181161003 100 -0.82816489384340308;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.98978021828605522 1 1 0.98970822020213189;
	setAttr -s 4 ".kiy[0:3]"  -0.14260126047692886 0 0 -0.14310010085366243;
	setAttr -s 4 ".kox[0:3]"  0.98978021832877661 1 1 0.98970822064550623;
	setAttr -s 4 ".koy[0:3]"  -0.14260126018040431 0 0 -0.14310009778719912;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_Controller_Upper_Mid_rotateZ";
	rename -uid "1FACF607-45EB-FD96-9894-E89F22BF1740";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.075261861736501262 29 -0.19229242611693328
		 50 0.1694541283744091 72 0.476933584084063 100 -0.075261861736501262;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "R_Foot_Controller_rotateX";
	rename -uid "7F9146F2-4AA8-9C5C-C0B4-63AFAE26CB57";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "R_Foot_Controller_rotateY";
	rename -uid "9CC6317F-44A8-8A8F-6927-0BB297F845F1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -15.458372838255475 25 -15.458372838255475
		 50 -15.458372838255475 100 -15.458372838255475;
createNode animCurveTA -n "R_Foot_Controller_rotateZ";
	rename -uid "E9DAF18E-43C1-107E-2315-76A1B44AD965";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "Tail_Controller_Base_Tail_Curl_Side";
	rename -uid "398920B5-4F51-8280-5D6B-0C942E52A135";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 100 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTU -n "Tail_Controller_Base_Tail_Curl_Up";
	rename -uid "75264309-47EF-3B39-D937-42B0415F781D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 100 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTU -n "R_Upper_Top_Lip_Controller_visibility";
	rename -uid "61A50D7A-4160-AD2F-CAD0-398E22E6AE6C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "R_Upper_Top_Lip_Controller_translateX";
	rename -uid "1D8EACC4-4158-E0D1-AF1E-B78ADA1C4FBF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "R_Upper_Top_Lip_Controller_translateY";
	rename -uid "25065613-4700-59B2-AFBB-D1A193F903E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "R_Upper_Top_Lip_Controller_translateZ";
	rename -uid "8C98C149-434B-8FF8-FDAB-819510EED8B9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Upper_Top_Lip_Controller_scaleX";
	rename -uid "43FA97E7-45B1-4ABE-B752-70AF4D0271B5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Upper_Top_Lip_Controller_scaleY";
	rename -uid "BF517562-445C-DDD4-99A0-17943F975876";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Upper_Top_Lip_Controller_scaleZ";
	rename -uid "963E9EF7-4CFF-256A-D228-B380BB896261";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Upper_Mid_Lip_Controller_visibility";
	rename -uid "F856E9C4-4C23-03AD-9CA5-E4A5B6BF62BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "R_Upper_Mid_Lip_Controller_translateX";
	rename -uid "84869C8B-467F-0776-AFAE-B4BDD4EF2ADE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 25 0 50 0 65 0.0036630595604714997 78 0.00015667163832584524
		 100 0;
createNode animCurveTL -n "R_Upper_Mid_Lip_Controller_translateY";
	rename -uid "A5D24C16-41D5-A2DC-0207-D6B0C08B3F39";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 25 0 50 0 65 0.04641532342737148 78 -0.0054428410950596792
		 100 0;
createNode animCurveTL -n "R_Upper_Mid_Lip_Controller_translateZ";
	rename -uid "B8FF18CC-4081-8479-E6CA-95B087E67681";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 25 0 50 0 65 -0.0055144297370621001
		 78 -0.0022176691857955177 100 0;
createNode animCurveTU -n "R_Upper_Mid_Lip_Controller_scaleX";
	rename -uid "5A1B6550-4453-C6EE-705E-0C943717CAB1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Upper_Mid_Lip_Controller_scaleY";
	rename -uid "50D7E7F1-4EA0-11AE-5B03-328864319A57";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Upper_Mid_Lip_Controller_scaleZ";
	rename -uid "61565989-4FD6-D980-7BFE-89B10E65DCA8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Lower_Top_Lip_Controller_visibility";
	rename -uid "22D002AD-4107-57A7-AB7C-ECB3259A5FAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "R_Lower_Top_Lip_Controller_translateX";
	rename -uid "AF0D3E5A-493F-CC5B-A651-27925E15B8E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "R_Lower_Top_Lip_Controller_translateY";
	rename -uid "031C86EE-4D54-51EF-8E5D-22918FF5091B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "R_Lower_Top_Lip_Controller_translateZ";
	rename -uid "A1D63285-466C-185A-F5C5-64AF585E42BE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Lower_Top_Lip_Controller_scaleX";
	rename -uid "F809D942-4E19-44CA-0748-4190A7C82DA2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Lower_Top_Lip_Controller_scaleY";
	rename -uid "497A7993-405F-FCB4-30ED-B7A8B12A0896";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Lower_Top_Lip_Controller_scaleZ";
	rename -uid "538EBBAB-4226-26C9-8D5C-9095255F86BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Lower_Mid_Lip_Controller_visibility";
	rename -uid "9060632D-4847-2C5E-342D-1486CF28C3EF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "R_Lower_Mid_Lip_Controller_translateX";
	rename -uid "B9D27464-420B-6C98-04A2-1C9D9B1A6611";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "R_Lower_Mid_Lip_Controller_translateY";
	rename -uid "1F70AA81-48E6-0EAA-7CEF-7DA97BF1E5F6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "R_Lower_Mid_Lip_Controller_translateZ";
	rename -uid "D1C9C665-4E8A-8EF1-BCF0-A08EC19587B0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Lower_Mid_Lip_Controller_scaleX";
	rename -uid "6DC0846A-4B61-77BE-63CC-9BA126045DD5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Lower_Mid_Lip_Controller_scaleY";
	rename -uid "76147E2C-4AFA-BED1-F86D-31A0D76875CB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Lower_Mid_Lip_Controller_scaleZ";
	rename -uid "E73CFEFB-4027-6D4F-7E20-74A8CC11C1B7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "R_Upper_Bottom_Lip_Controller_visibility";
	rename -uid "0864622A-494B-CD66-AA50-33B9928D12F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 26 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "R_Upper_Bottom_Lip_Controller_translateX";
	rename -uid "9CB07CB1-43F9-9364-3429-2F97A60EED29";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 12 0.0051002990248322179 22 0.0051002990248322179
		 26 0 38 0.011014762326281707 50 0 71 0.0077931246263617813 80 0 82 0.0035972099160042318
		 93 -0.0027112541003089471 100 0;
	setAttr -s 11 ".kit[2:10]"  1 18 18 18 18 18 18 18 
		18;
	setAttr -s 11 ".kot[2:10]"  1 18 18 18 18 18 18 18 
		18;
	setAttr -s 11 ".kix[2:10]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[2:10]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[2:10]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[2:10]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_Upper_Bottom_Lip_Controller_translateY";
	rename -uid "1A60E188-416F-B486-F929-1E89E95811E8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 12 0.072518419273180951 22 0.072518419273180951
		 26 0 38 0.13045948597725127 50 0 71 0.10039884258801127 80 0 82 0.052526636503495913
		 93 -0.043446416673866797 100 0;
	setAttr -s 11 ".kit[2:10]"  1 18 18 18 18 18 18 18 
		18;
	setAttr -s 11 ".kot[2:10]"  1 18 18 18 18 18 18 18 
		18;
	setAttr -s 11 ".kix[2:10]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[2:10]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[2:10]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[2:10]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_Upper_Bottom_Lip_Controller_translateZ";
	rename -uid "19EF4484-4FF2-7CF3-9D47-1188254ED15C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 12 -2.5094984798733057e-05 22 -2.5094984798733057e-05
		 26 0 38 -0.012559850135529375 50 0 71 -0.011201295900729462 80 0 82 -0.0035174366458633139
		 93 0.00085165271804100605 100 0;
	setAttr -s 11 ".kit[2:10]"  1 18 18 18 18 18 18 18 
		18;
	setAttr -s 11 ".kot[2:10]"  1 18 18 18 18 18 18 18 
		18;
	setAttr -s 11 ".kix[2:10]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[2:10]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[2:10]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[2:10]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_Upper_Bottom_Lip_Controller_scaleX";
	rename -uid "9BC51795-4D35-D5BB-B491-6C809D43ECC2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 26 1 50 1 100 1;
createNode animCurveTU -n "R_Upper_Bottom_Lip_Controller_scaleY";
	rename -uid "E82DC17C-474E-C00E-24F6-2EA716FE3DEB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 26 1 50 1 100 1;
createNode animCurveTU -n "R_Upper_Bottom_Lip_Controller_scaleZ";
	rename -uid "F0D1A057-4CD9-C17E-42D3-A8A167B9AFBE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 26 1 50 1 100 1;
createNode animCurveTU -n "R_Lower_Bottom_Lip_Controller_visibility";
	rename -uid "CC95F66A-4F1A-E9F6-C1BA-0A8ED4CF41BE";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 25 1 50 1 60 1 100 1;
	setAttr -s 5 ".kit[0:4]"  9 9 9 1 9;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
createNode animCurveTL -n "R_Lower_Bottom_Lip_Controller_translateX";
	rename -uid "4AA2E1A8-4F96-3794-050B-74A70B23F018";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  1 0 12 -0.0057689464691605356 18 0.0053687759194202917
		 25 0 42 0.0058532929991843945 50 0 60 0 67 0.004280470788217498 76 0 100 0;
	setAttr -s 10 ".kit[6:9]"  1 18 18 18;
	setAttr -s 10 ".kot[6:9]"  1 18 18 18;
	setAttr -s 10 ".kix[6:9]"  1 1 1 1;
	setAttr -s 10 ".kiy[6:9]"  0 0 0 0;
	setAttr -s 10 ".kox[6:9]"  1 1 1 1;
	setAttr -s 10 ".koy[6:9]"  0 0 0 0;
createNode animCurveTL -n "R_Lower_Bottom_Lip_Controller_translateY";
	rename -uid "83C61210-413F-D002-1210-7F851F670AFA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  1 0 12 -0.042021091720960538 18 0.038727186901444749
		 25 0 42 0.074076465442650716 50 0 60 0 67 0.070002578079150127 76 0 100 0;
	setAttr -s 10 ".kit[6:9]"  1 18 18 18;
	setAttr -s 10 ".kot[6:9]"  1 18 18 18;
	setAttr -s 10 ".kix[6:9]"  1 1 1 1;
	setAttr -s 10 ".kiy[6:9]"  0 0 0 0;
	setAttr -s 10 ".kox[6:9]"  1 1 1 1;
	setAttr -s 10 ".koy[6:9]"  0 0 0 0;
createNode animCurveTL -n "R_Lower_Bottom_Lip_Controller_translateZ";
	rename -uid "E2D2A0F7-45CE-5772-E318-C78A1012EB67";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  1 0 12 -0.0012199125650594535 18 -0.0053114756979399935
		 25 0 42 -0.012086461256433392 50 0 60 0 67 -0.009703650095781351 76 0 100 0;
	setAttr -s 10 ".kit[6:9]"  1 18 18 18;
	setAttr -s 10 ".kot[6:9]"  1 18 18 18;
	setAttr -s 10 ".kix[6:9]"  1 1 1 1;
	setAttr -s 10 ".kiy[6:9]"  0 0 0 0;
	setAttr -s 10 ".kox[6:9]"  1 1 1 1;
	setAttr -s 10 ".koy[6:9]"  0 0 0 0;
createNode animCurveTU -n "R_Lower_Bottom_Lip_Controller_scaleX";
	rename -uid "5A1C58CF-406C-B5E3-3ACA-BDB1056F9377";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 25 1 50 1 60 1 100 1;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTU -n "R_Lower_Bottom_Lip_Controller_scaleY";
	rename -uid "8B3E2189-4228-B51D-213F-B7878465FA5D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 25 1 50 1 60 1 100 1;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTU -n "R_Lower_Bottom_Lip_Controller_scaleZ";
	rename -uid "48AEAD98-45BC-19EA-F2C7-3199CB967D6F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 25 1 50 1 60 1 100 1;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTU -n "L_Upper_Bottom_Lip_Controller_visibility";
	rename -uid "1814C06D-4499-75F2-DD40-90A28C43749E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 19 1 25 1 50 1 100 1;
	setAttr -s 5 ".kot[0:4]"  5 5 5 5 5;
createNode animCurveTL -n "L_Upper_Bottom_Lip_Controller_translateX";
	rename -uid "6F237330-4C77-52E7-4655-03A588FE33D6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 12 0.014749951078187683 19 0.012755988642947929
		 25 0 40 0.012885117904303672 50 0 71 0.0062118461237400087 80 0 82 0.0035972099160042318
		 93 -0.0027112541003089471 100 0;
createNode animCurveTL -n "L_Upper_Bottom_Lip_Controller_translateY";
	rename -uid "34FDC02E-4D7C-7A44-9099-F3ABC82D3594";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 12 0.20972165187551883 19 0.18137056830381693
		 25 0 40 0.15176764155628999 50 0 71 0.080027227981016164 80 0 82 0.052526636503495913
		 93 -0.043446416673866797 100 0;
createNode animCurveTL -n "L_Upper_Bottom_Lip_Controller_translateZ";
	rename -uid "19926F60-4C25-B7F1-DBF2-69B0AD00EAA0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 12 -7.2574136590388875e-05 19 -6.2763249668520201e-05
		 25 0 40 -0.017175921708966611 50 0 71 -0.0089284760423875634 80 0 82 -0.0035174366458633139
		 93 0.00085165271804100605 100 0;
createNode animCurveTU -n "L_Upper_Bottom_Lip_Controller_scaleX";
	rename -uid "13C51B72-4076-169F-7916-7786A7E4F13A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 19 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Upper_Bottom_Lip_Controller_scaleY";
	rename -uid "99BEE59E-40BC-3609-2CDC-00A530F4B48A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 19 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Upper_Bottom_Lip_Controller_scaleZ";
	rename -uid "3D4FAD00-4D4B-581A-8C62-8D83B5B4479E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 19 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Lower_Bottom_Lip_Controller_visibility";
	rename -uid "C4F0E0B9-40F7-4411-F07F-61BF227B3878";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 25 1 50 1 60 1 100 1;
	setAttr -s 5 ".kit[0:4]"  9 9 9 1 9;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
createNode animCurveTL -n "L_Lower_Bottom_Lip_Controller_translateX";
	rename -uid "72D06AD7-40F0-C5D6-A00E-05AFFA6B9879";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  1 0 12 -0.009940102136872497 18 0.0028357054178903404
		 25 0 42 0.0058532929991843945 50 0 60 0 67 0.004280470788217498 76 0 100 0;
	setAttr -s 10 ".kit[6:9]"  1 18 18 18;
	setAttr -s 10 ".kot[6:9]"  1 18 18 18;
	setAttr -s 10 ".kix[6:9]"  1 1 1 1;
	setAttr -s 10 ".kiy[6:9]"  0 0 0 0;
	setAttr -s 10 ".kox[6:9]"  1 1 1 1;
	setAttr -s 10 ".koy[6:9]"  0 0 0 0;
createNode animCurveTL -n "L_Lower_Bottom_Lip_Controller_translateY";
	rename -uid "3158735A-49B8-1D57-DAAC-2189F3B1E72B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  1 0 12 -0.072403851525075988 18 0.020276263765053832
		 25 0 42 0.074076465442650716 50 0 60 0 67 0.070002578079150127 76 0 100 0;
	setAttr -s 10 ".kit[6:9]"  1 18 18 18;
	setAttr -s 10 ".kot[6:9]"  1 18 18 18;
	setAttr -s 10 ".kix[6:9]"  1 1 1 1;
	setAttr -s 10 ".kiy[6:9]"  0 0 0 0;
	setAttr -s 10 ".kox[6:9]"  1 1 1 1;
	setAttr -s 10 ".koy[6:9]"  0 0 0 0;
createNode animCurveTL -n "L_Lower_Bottom_Lip_Controller_translateZ";
	rename -uid "5C94F52A-4FEC-C274-9B08-4BB1E0A56479";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  1 0 12 -0.0021019532005627155 18 -0.0058032826066753123
		 25 0 42 -0.012086461256433392 50 0 60 0 67 -0.009703650095781351 76 0 100 0;
	setAttr -s 10 ".kit[6:9]"  1 18 18 18;
	setAttr -s 10 ".kot[6:9]"  1 18 18 18;
	setAttr -s 10 ".kix[6:9]"  1 1 1 1;
	setAttr -s 10 ".kiy[6:9]"  0 0 0 0;
	setAttr -s 10 ".kox[6:9]"  1 1 1 1;
	setAttr -s 10 ".koy[6:9]"  0 0 0 0;
createNode animCurveTU -n "L_Lower_Bottom_Lip_Controller_scaleX";
	rename -uid "66E9CAE8-48AC-52E4-C68E-2CAD57A0484C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 25 1 50 1 60 1 100 1;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTU -n "L_Lower_Bottom_Lip_Controller_scaleY";
	rename -uid "56B23EF2-4A66-9601-C200-79ADCB46EA6A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 25 1 50 1 60 1 100 1;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTU -n "L_Lower_Bottom_Lip_Controller_scaleZ";
	rename -uid "8880DA44-4593-26E0-C5AE-F48EF04DD802";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 25 1 50 1 60 1 100 1;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTU -n "L_Upper_Mid_Lip_Controller_visibility";
	rename -uid "AB9106D4-440C-F783-56B5-FE8BA88D35E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "L_Upper_Mid_Lip_Controller_translateX";
	rename -uid "5A9A603E-4998-D325-3812-A39028611D27";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Upper_Mid_Lip_Controller_translateY";
	rename -uid "8CD8F2CA-4080-0D57-5D2D-52AA1AD32F75";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Upper_Mid_Lip_Controller_translateZ";
	rename -uid "409D9583-4C59-65C3-9160-59BD695EFE58";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Upper_Mid_Lip_Controller_scaleX";
	rename -uid "DF4BC398-480F-C361-22BA-369802402188";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Upper_Mid_Lip_Controller_scaleY";
	rename -uid "250203F1-40DD-9D0E-5EE8-E19CB511E039";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Upper_Mid_Lip_Controller_scaleZ";
	rename -uid "B98363CE-4A53-1C48-2697-7BBAAB15F4E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Lower_Mid_Lip_Controller_visibility";
	rename -uid "926E5FC5-4F0B-DEA2-FD24-8DBDAC07AFAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "L_Lower_Mid_Lip_Controller_translateX";
	rename -uid "B142DD54-43F9-EC53-D8CA-5782B5BF8570";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Lower_Mid_Lip_Controller_translateY";
	rename -uid "D2413B84-4AE5-58F9-C5C6-968236452A34";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Lower_Mid_Lip_Controller_translateZ";
	rename -uid "2F84A9D8-49BC-A755-4411-FA8FA4754056";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Lower_Mid_Lip_Controller_scaleX";
	rename -uid "4418DE66-4D22-1169-3EC2-6F9E13C6FF60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Lower_Mid_Lip_Controller_scaleY";
	rename -uid "32C82251-4A49-C933-AAD5-37AFFB4FE6B2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Lower_Mid_Lip_Controller_scaleZ";
	rename -uid "F8D473CB-4E5A-4851-B4E8-128A44E31930";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Upper_Top_Lip_Controller_visibility";
	rename -uid "AAF5B771-4E35-D675-1F39-069C93D37171";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "L_Upper_Top_Lip_Controller_translateX";
	rename -uid "578F6188-40CE-CC4A-41F3-F49017F0D784";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Upper_Top_Lip_Controller_translateY";
	rename -uid "E7C4CE8E-4ACD-A59E-3B10-E393B9CAEAF8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Upper_Top_Lip_Controller_translateZ";
	rename -uid "9FD932DD-4A8A-6675-4889-0D97C823A1DB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Upper_Top_Lip_Controller_scaleX";
	rename -uid "58EBEF38-4E95-C1A4-09B0-E199F06A6AAC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Upper_Top_Lip_Controller_scaleY";
	rename -uid "639ED3BC-431D-9589-A3D8-099C0B0FCBB2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Upper_Top_Lip_Controller_scaleZ";
	rename -uid "CAE93029-4993-8333-B2B3-DBA01A680AE7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Lower_Top_Lip_Controller_visibility";
	rename -uid "193DA9C8-4B3B-9293-6551-C29CD614F6FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "L_Lower_Top_Lip_Controller_translateX";
	rename -uid "AA4E898A-48F4-3133-5823-B8B800FA7DFF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Lower_Top_Lip_Controller_translateY";
	rename -uid "4A5016BC-4C0E-4EF4-5F5C-57B61271666B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Lower_Top_Lip_Controller_translateZ";
	rename -uid "896A6AEE-4B9F-D228-05AF-CDB93E2764ED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Lower_Top_Lip_Controller_scaleX";
	rename -uid "0FE34568-4F5E-1C37-A4EF-A1B3F3A4A8CE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Lower_Top_Lip_Controller_scaleY";
	rename -uid "774DC8D9-4A20-D194-D766-47B9D0DDFB60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Lower_Top_Lip_Controller_scaleZ";
	rename -uid "19C1595F-4F11-2D37-AA85-C88E68908B52";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 25 1 50 1 100 1;
createNode animCurveTU -n "L_Eyeball_Controller_visibility";
	rename -uid "E26CA552-4C48-56B0-1D77-78AE34791344";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 16 1 32 1 50 1 54 1 63 1 100 1;
	setAttr -s 7 ".kit[0:6]"  9 9 9 9 9 1 9;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "L_Eyeball_Controller_translateX";
	rename -uid "5A6B974C-4904-BC74-9E01-51B49745DC19";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTL -n "L_Eyeball_Controller_translateY";
	rename -uid "57A99E46-4234-6E55-EC1B-D1A1445CB824";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 8.8817841970012523e-16 16 8.8817841970012523e-16
		 32 8.8817841970012523e-16 50 8.8817841970012523e-16 54 8.8817841970012523e-16 63 8.8817841970012523e-16
		 100 8.8817841970012523e-16;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTL -n "L_Eyeball_Controller_translateZ";
	rename -uid "3B8053E0-4CC6-CAD9-3E5C-2C97DE987A83";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTU -n "L_Eyeball_Controller_Top_Eyelid";
	rename -uid "7E8F70D4-49AE-0AD6-50DC-85B98A0C2FAD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 16 0 23 10 32 0 50 0 54 0 58 10 63 0
		 100 0;
	setAttr -s 9 ".kit[7:8]"  1 18;
	setAttr -s 9 ".kot[5:8]"  1 18 18 18;
	setAttr -s 9 ".kix[7:8]"  1 1;
	setAttr -s 9 ".kiy[7:8]"  0 0;
	setAttr -s 9 ".kox[5:8]"  1 1 1 1;
	setAttr -s 9 ".koy[5:8]"  0 0 0 0;
createNode animCurveTU -n "L_Eyeball_Controller_Bottom_Eyelid";
	rename -uid "0B4BFE7A-4346-E15B-C877-AF9A98CEBED6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 16 0 23 10 32 0 50 0 54 0 58 10 63 0
		 100 0;
	setAttr -s 9 ".kit[7:8]"  1 18;
	setAttr -s 9 ".kot[5:8]"  1 18 18 18;
	setAttr -s 9 ".kix[7:8]"  1 1;
	setAttr -s 9 ".kiy[7:8]"  0 0;
	setAttr -s 9 ".kox[5:8]"  1 1 1 1;
	setAttr -s 9 ".koy[5:8]"  0 0 0 0;
createNode animCurveTU -n "L_Eyeball_Controller_Both_Eyelids";
	rename -uid "97B650E8-4CB7-180C-457B-7E9F45E61DB6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Eyeball_Controller_visibility";
	rename -uid "F5ED794E-4435-7696-5F92-06B5DEE70F44";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 16 1 32 1 50 1 54 1 63 1 100 1;
	setAttr -s 7 ".kit[0:6]"  9 9 9 9 9 1 9;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_Eyeball_Controller_translateX";
	rename -uid "3E4B59EC-48F2-4F29-7141-539F5C18D9D1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTL -n "R_Eyeball_Controller_translateY";
	rename -uid "24B5D5C7-49FE-F061-C5AD-C88A4F0A8AF8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 8.8817841970012523e-16 16 8.8817841970012523e-16
		 32 8.8817841970012523e-16 50 8.8817841970012523e-16 54 8.8817841970012523e-16 63 8.8817841970012523e-16
		 100 8.8817841970012523e-16;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTL -n "R_Eyeball_Controller_translateZ";
	rename -uid "08FD10DD-4F76-2735-EC16-32BA590F6EAA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 16 0 32 0 50 0 54 0 63 0 100 0;
	setAttr -s 7 ".kit[5:6]"  1 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTU -n "R_Eyeball_Controller_Top_Eyelid";
	rename -uid "36610908-427E-F057-6BBD-DB82E4DE08E1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 16 0 23 10 32 0 50 0 54 0 58 10 63 0
		 100 0;
	setAttr -s 9 ".kit[7:8]"  1 18;
	setAttr -s 9 ".kot[5:8]"  1 18 18 18;
	setAttr -s 9 ".kix[7:8]"  1 1;
	setAttr -s 9 ".kiy[7:8]"  0 0;
	setAttr -s 9 ".kox[5:8]"  1 1 1 1;
	setAttr -s 9 ".koy[5:8]"  0 0 0 0;
createNode animCurveTU -n "R_Eyeball_Controller_Bottom_Eyelid";
	rename -uid "308DA8DB-430D-FAAE-5EBA-659C90E23EA9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 16 0 23 10 32 0 50 0 54 0 58 10 63 0
		 100 0;
	setAttr -s 9 ".kit[7:8]"  1 18;
	setAttr -s 9 ".kot[5:8]"  1 18 18 18;
	setAttr -s 9 ".kix[7:8]"  1 1;
	setAttr -s 9 ".kiy[7:8]"  0 0;
	setAttr -s 9 ".kox[5:8]"  1 1 1 1;
	setAttr -s 9 ".koy[5:8]"  0 0 0 0;
createNode animCurveTU -n "R_Eyeball_Controller_Both_Eyelids";
	rename -uid "47D57155-432F-48C9-7396-F99C7973634A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Shoulder_Controller_translateY";
	rename -uid "7E91447C-44AA-DF41-3B60-9F87D075BCF1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0.094076266952307017 50 0 75 0.094076266952307017
		 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTL -n "L_Shoulder_Controller_translateZ";
	rename -uid "1B66DD6B-4A66-8F44-9AC9-2B998C742281";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  1 1;
	setAttr -s 5 ".kiy[3:4]"  0 0;
	setAttr -s 5 ".kox[3:4]"  1 1;
	setAttr -s 5 ".koy[3:4]"  0 0;
createNode animCurveTL -n "R_Shoulder_Contoller_translateX";
	rename -uid "68CCC39F-4BCE-9877-789C-4CBE828ED63E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kot[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTL -n "R_Shoulder_Contoller_translateY";
	rename -uid "A8EBDBDD-4DAC-72FE-A651-8EA47F005504";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.010395540260898169 25 0.082571945641548974
		 50 -0.010395540260898169 75 0.082571945641548974 100 -0.010395540260898169;
	setAttr -s 5 ".kit[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kot[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTL -n "R_Shoulder_Contoller_translateZ";
	rename -uid "53B9F164-44A0-C9E3-3424-A2A9F549B29E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0 50 0 75 0 100 0;
	setAttr -s 5 ".kit[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kot[0:4]"  18 18 1 1 1;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTL -n "Head_Controller_translateX";
	rename -uid "B49C3DB1-47E9-25D0-1F2D-798BA9506331";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 47 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTL -n "Head_Controller_translateY";
	rename -uid "03CC6ECA-4740-A94B-C24E-F5ACA3108516";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.9522899999999993e-05 47 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTL -n "Head_Controller_translateZ";
	rename -uid "FB0161CD-40D9-1F94-8251-3694A16BD330";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 47 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTL -n "Head_Base_Controller_translateX";
	rename -uid "BC86D455-41F7-0B5B-AEF0-5DACAB9701D6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Head_Base_Controller_translateY";
	rename -uid "8EBFE5D9-42D6-82F7-852C-C7B28EF08FF1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Head_Base_Controller_translateZ";
	rename -uid "BA694271-4571-67BE-8852-A9920C0F9365";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Neck_Upper_Mid_Controller_translateX";
	rename -uid "3BD5B719-4DB5-F6C0-EE68-40A5C59853FA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Neck_Upper_Mid_Controller_translateY";
	rename -uid "1F6BF147-404F-9599-525B-A7857FC1C7CE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Neck_Upper_Mid_Controller_translateZ";
	rename -uid "F09C94E6-4EB4-12BA-02B5-F9939B53D77B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Neck_Lower_Mid_Controller_translateX";
	rename -uid "235C1D48-4F06-B1F0-8D8C-44934E5282F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Neck_Lower_Mid_Controller_translateY";
	rename -uid "429B54DD-4588-25B1-71F1-B9AA1A5DA45A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Neck_Lower_Mid_Controller_translateZ";
	rename -uid "AABC7FAD-4EB5-8E2D-8B78-FCB4CB3E9189";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Neck_Base_Contoller_translateX";
	rename -uid "E0D0543C-4984-CBC7-C11B-83B623998BE8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 54 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTL -n "Neck_Base_Contoller_translateY";
	rename -uid "980C0C00-4F72-59BD-1603-44A18091E724";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 54 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTL -n "Neck_Base_Contoller_translateZ";
	rename -uid "762DB554-4995-F492-EE80-E7ABA2911CE9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 54 0 100 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTL -n "Upper_Body_Contraint_translateX";
	rename -uid "45BAE032-4F40-30A1-6FF9-BA854CFD8ED9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Upper_Body_Contraint_translateY";
	rename -uid "C332EECE-4B1A-78C2-3CF4-C38944BE0C34";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Upper_Body_Contraint_translateZ";
	rename -uid "C45CDBB0-4EB4-4D36-B88E-6784760F9451";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Middle_Waist_Controller_translateX";
	rename -uid "24BD7280-45D1-DB3A-16C8-D2946BA36A97";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Middle_Waist_Controller_translateY";
	rename -uid "CF35251B-41B5-31D2-1EFE-D881D5A9CA95";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "Middle_Waist_Controller_translateZ";
	rename -uid "1CC73ADF-4E6E-512E-B878-35B35E83FE50";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "R_Foot_Controller_translateX";
	rename -uid "4EC82C79-4CA9-B124-A4BE-159277F398DD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -0.33373086870771229 25 -0.33373086870771229
		 50 -0.33373086870771229 100 -0.33373086870771229;
createNode animCurveTL -n "R_Foot_Controller_translateY";
	rename -uid "11C6966D-4493-7B13-6FB9-C1A1FACDD074";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "R_Foot_Controller_translateZ";
	rename -uid "4BC36E13-4B09-6A4F-838E-55AFF9824D87";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Foot_Controller_R_Foot_Spread";
	rename -uid "EA62EF42-4F0A-3879-0550-F8B362430B2D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Foot_Controller_R_Foot_Curl";
	rename -uid "C00D90DD-441B-3BF9-42B5-0B8C3944A47A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Foot_Controller_R_Foot_Roll";
	rename -uid "121670B1-41BD-8ABA-92BF-FFA488DF18D1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Foot_Controller_R_Left_Toe_Curl";
	rename -uid "BD899A0D-4516-7A75-9899-98B0857CAF80";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Foot_Controller_R_Right_Toe_Curl";
	rename -uid "5D843807-4011-AD7D-50B1-6A87D6091215";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Foot_Controller_R_Mid_Toe_Curl";
	rename -uid "6D1C4292-480C-F244-EA31-D5A8D172EA16";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Foot_Controller_R_Left_Toe_Spread";
	rename -uid "3DDC2C5B-47C7-3273-11D0-F1A06055AC35";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "R_Foot_Controller_R_Right_Toe_Spread";
	rename -uid "95A5FD2D-493D-22B8-25D5-C6A3D12817F1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Foot_Controller_translateX";
	rename -uid "CC299166-4606-1DB8-EC5D-DBABEC9A730F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Foot_Controller_translateY";
	rename -uid "0086AFE9-4FB1-8FC8-1124-9F97A6590A9B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTL -n "L_Foot_Controller_translateZ";
	rename -uid "4789F637-4CD0-EBC4-5680-DBA0CE03E9A0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -0.32144552295996442 25 -0.32144552295996442
		 50 -0.32144552295996442 100 -0.32144552295996442;
createNode animCurveTU -n "L_Foot_Controller_L_Foot_Spread";
	rename -uid "7BB1190B-47BE-ABA7-A08F-1293A6CEE1A6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Foot_Controller_L_Foot_Curl";
	rename -uid "1C66A93F-4F74-6EEA-426F-0CAF5A088B32";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Foot_Controller_L_Foot_Roll";
	rename -uid "7DADE5D6-4474-1941-C015-85B2015AC28C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Foot_Controller_L_Left_Toe_Curl";
	rename -uid "392FC7DF-4FF7-BD7C-21F5-AA9D0D26B9B3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Foot_Controller_L_Right_Toe_Curl";
	rename -uid "8C6B2E62-4327-31DC-F20D-F38E6E538E6D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Foot_Controller_L_Mid_Toe_Curl";
	rename -uid "58EFBD6A-4386-DA3E-6788-46825D845584";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Foot_Controller_L_Left_Toe_Spread";
	rename -uid "8A442D3D-4F5B-FCA6-8801-5A80D1D58E78";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTU -n "L_Foot_Controller_L_Right_Toe_Spread";
	rename -uid "438EA1A2-4312-1F6B-1B71-88B6F5FA2ABB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 25 0 50 0 100 0;
createNode animCurveTA -n "Main_Body_Cog_Controller_rotateX";
	rename -uid "0E39F8AD-48D5-D093-AFC7-70A9D3A575EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 50 0 100 0;
createNode animCurveTA -n "Main_Body_Cog_Controller_rotateY";
	rename -uid "61AC7DC3-4F32-1793-AF9A-8FB68E842F1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.0000000000000004 50 -3.0000000000000004
		 100 -3.0000000000000004;
createNode animCurveTA -n "Main_Body_Cog_Controller_rotateZ";
	rename -uid "7733C0A8-4CF1-F4EF-A649-DA906631A415";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 50 0 100 0;
createNode animCurveTL -n "Main_Body_Cog_Controller_translateZ";
	rename -uid "62BEAFF5-48FA-96A2-538B-03BB2D0CA571";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 14 0.0063931800050936634 25 0.0078583558124796227
		 50 0 62 0.0067872361202231642 75 0.0088406502890396865 100 0;
createNode animCurveTL -n "Main_Body_Cog_Controller_translateY";
	rename -uid "91BBED2D-4221-85C8-2DA6-7DA5C9EBA99D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.043761342199980083 25 -0.1371796546807699
		 50 -0.043761342199980083 75 -0.13681087822016572 100 -0.043761342199980083;
createNode animCurveTL -n "Main_Body_Cog_Controller_translateX";
	rename -uid "16BBC90C-47FC-1926-796D-41947AD29BB9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.047822440793083398 14 -0.053106044988926147
		 50 -0.047822440793083398 62 -0.04319928712172097 100 -0.047822440793083398;
createNode animCurveTA -n "Lower_Jaw_Controller_rotateX";
	rename -uid "A1090516-4A99-643C-D08A-C9BB0B014AE4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  1 -10 4 -9.6205627947924519 18 2.6990616234600688
		 21 2.2780124118536538 25 -9.9928719087116828 33 -9.9926807220147644 42 2.436599403516909
		 45 -0.23482743958714677 51 -9.3986545410390256 57 -9.5342280431808284 66 1.6321962732706139
		 71 0.35721535990989778 73 -1.4255837735893295 77 -9.9332724545939932 82 -9.9623831483570466
		 86 -4.5484059586613936 90 -5.2464530425654283 94 -9.7473739780659479 100 -10;
	setAttr -s 19 ".kit[18]"  1;
	setAttr -s 19 ".kot[18]"  1;
	setAttr -s 19 ".kix[18]"  1;
	setAttr -s 19 ".kiy[18]"  0;
	setAttr -s 19 ".kox[18]"  1;
	setAttr -s 19 ".koy[18]"  0;
createNode animCurveTA -n "Lower_Jaw_Controller_rotateY";
	rename -uid "3908BD6E-408B-EE4C-E289-53AA7616A533";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  1 0 9 0.27866625726683025 14 0.8208698801230907
		 22 -5.5343286637071563 25 -1.1475352487421655 33 -0.016808162176119999 42 0.90197571789806041
		 47 3.5030381194802467 51 2.4451246360504064 57 1.8938706020458596 61 4.3152440841008239
		 71 -2.8274958548218296 73 -3.3097715099354459 77 -1.4248884580845205 82 -1.8841239556069034
		 84 0.96501289195257867 86 0.38651451969041373 90 -3.9053674591555545 100 0;
	setAttr -s 19 ".kit[18]"  1;
	setAttr -s 19 ".kot[18]"  1;
	setAttr -s 19 ".kix[18]"  1;
	setAttr -s 19 ".kiy[18]"  0;
	setAttr -s 19 ".kox[18]"  1;
	setAttr -s 19 ".koy[18]"  0;
createNode animCurveTA -n "Lower_Jaw_Controller_rotateZ";
	rename -uid "FD393506-45CB-5C9F-973D-EA835A7BD7F2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  1 0 9 -0.11145844051615643 14 5.9109865400285031
		 21 -0.75684466184017007 22 -5.9111962752614886 25 -0.7700776383272151 33 -0.93274396789529312
		 42 -0.48371172091691222 45 0.22112514527530017 51 1.2198029809240019 57 1.1967442058424034
		 61 5.3734276601014184 71 -4.489716373372473 73 -4.4391805929213133 77 -2.5492355242507299
		 82 -1.8959459023920961 86 2.1412316914492635 90 -4.1614052204693577 100 0;
	setAttr -s 19 ".kit[18]"  1;
	setAttr -s 19 ".kot[18]"  1;
	setAttr -s 19 ".kix[18]"  1;
	setAttr -s 19 ".kiy[18]"  0;
	setAttr -s 19 ".kox[18]"  1;
	setAttr -s 19 ".koy[18]"  0;
createNode animCurveTU -n "Lower_Jaw_Controller_visibility";
	rename -uid "942AE93B-4F5F-CED1-287C-A69A394AAF00";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 57 1 82 1 100 1;
	setAttr -s 4 ".kit[0:3]"  9 9 9 1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
createNode animCurveTU -n "Lower_Jaw_Controller_scaleX";
	rename -uid "38FB3F17-41F7-E9F7-D7DD-7F86323E849B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 57 1 82 1 100 1;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "Lower_Jaw_Controller_scaleY";
	rename -uid "15039F34-43EC-49CE-F785-5AA2EFAF9B68";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 57 1 82 1 100 1;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "Lower_Jaw_Controller_scaleZ";
	rename -uid "5FB12925-4F7F-C0C3-D57B-50A138C7EC72";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 57 1 82 1 100 1;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "Lower_Jaw_Controller_Tongue_Curl_Up";
	rename -uid "6FA6A85E-4874-9C7D-4387-C296CE4F5267";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 57 0 82 0 100 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "Lower_Jaw_Controller_Tongue_Curl_Tip";
	rename -uid "D874D2B5-416D-7DA4-7F45-21B6925A5A5B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 57 0 82 0 100 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "3573955D-442D-EDA9-A0D6-C99E07E5D519";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 0\n            -height 0\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 0\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 0\n            -ikHandles 0\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 781\n            -height 424\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 0\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 0\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 0\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 781\n            -height 424\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -selectionOrder \"display\" \n                -expandAttribute 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 1\n                -displayValues 0\n"
		+ "                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 50 100 -ps 2 50 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"top\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 0\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 781\\n    -height 424\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"top\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 0\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 781\\n    -height 424\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 0\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 781\\n    -height 424\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 0\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 781\\n    -height 424\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "374CED82-49F4-23FA-8335-E9A61C5E0A16";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 100 -ast 1 -aet 250 ";
	setAttr ".st" 6;
createNode polyPlane -n "polyPlane1";
	rename -uid "FC3F5B9A-4DFB-9DBF-E278-E9967240AA1C";
	setAttr ".cuv" 2;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "720119BA-4368-989A-F741-F4833F669282";
	setAttr ".version" -type "string" "3.1.2";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "E513F90D-44FC-AF91-05BE-A8AE858A7753";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "611A8299-48CA-596D-B5EA-1591406A3CA0";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "D0D01CBD-40A8-CF06-DB17-239491BAFCE6";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode phong -n "Ground";
	rename -uid "D5426B4E-4F61-E547-7615-DD9ACBC904D5";
	setAttr ".it" -type "float3" 1 1 1 ;
createNode shadingEngine -n "phong1SG";
	rename -uid "4F19CD27-44B9-74EB-35CA-2EBF1BAE28CB";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "44B93D75-497E-DF6A-43FC-2FAB7A7CC1FD";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "BB96AC30-4AF4-9BF3-B45E-8D9D9670F6FF";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
	setAttr -s 2 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 54.285713195800781;
	setAttr ".tgi[0].ni[0].y" 190;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" -252.85714721679688;
	setAttr ".tgi[0].ni[1].y" 190;
	setAttr ".tgi[0].ni[1].nvs" 1923;
createNode animCurveTA -n "Tail_Controller_Tip_rotateZ";
	rename -uid "56BBA083-49CE-CFF6-1A1A-3E8E1C6D7FC5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.30171298435929356 29 -0.30446857651799358
		 50 0.28869220587066557 72 0.57554982124121923 100 -0.30171298435929356;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Tail_Controller_Tip_rotateY";
	rename -uid "4A4E7946-4D46-3AC9-19B8-26AD46D47F10";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -3.1622314278984307 29 -1.2288144883441046
		 50 3.0260221249583439 72 6.0079999822381653 100 -3.1622314278984307;
	setAttr -s 5 ".kit[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kot[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kix[0:4]"  0.97263291414869446 0.99782083616422756 
		0.9961398438714576 1 0.97228271829191437;
	setAttr -s 5 ".kiy[0:4]"  -0.23234718486484499 0.065981655909212872 
		0.087780473066325193 0 -0.23380828837080603;
	setAttr -s 5 ".kox[0:4]"  0.97263291375850203 0.99782083616422756 
		0.99613984387145771 1 0.97228273107275986;
	setAttr -s 5 ".koy[0:4]"  -0.23234718649823682 0.065981655909212872 
		0.087780473066325207 0 -0.2338082352221485;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_Controller_Tip_rotateX";
	rename -uid "F5A8DA4C-4004-575C-F0BC-A59776073C60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 5.4529746162355899 29 5.4533654318321041
		 50 5.4522718002639277 72 5.4825690221180592 100 5.4529746162355899;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode displayLayer -n "Floor";
	rename -uid "81BD84CC-4DAC-9EAC-5F06-EABBED6C6727";
	setAttr ".dt" 2;
	setAttr ".do" 2;
createNode reference -n "sharedReferenceNode";
	rename -uid "C8D660FF-4443-6F98-9686-CDBFB55D7C5F";
	setAttr ".ed" -type "dataReferenceEdits" 
		"sharedReferenceNode";
createNode animCurveTA -n "Tail_1_CNTRL_rotateX";
	rename -uid "1A8C3FB1-4A71-3786-AC68-6E8F97082208";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -9.9392472676555403 28 -12.545118565494985
		 50 -9.9535363445838865 78 -12.842943306130881 100 -9.9392472676555403;
createNode animCurveTA -n "Tail_1_CNTRL_rotateY";
	rename -uid "7560F338-42DF-6936-C587-FA92134FE70E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.1438824990133298 50 0.48823128981670394
		 100 4.1438824990133298;
createNode animCurveTA -n "Tail_1_CNTRL_rotateZ";
	rename -uid "0F04758C-4B6D-C9A7-6A4B-4D8E186C19D6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 50 0.53585031378998427 100 0;
createNode animCurveTU -n "Tail_1_CNTRL_Tail_Curl_Side";
	rename -uid "1C58CA3F-4E88-E168-1E5E-3FA35C45D06C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 100 0;
createNode animCurveTU -n "Tail_1_CNTRL_Tail_Curl_Up";
	rename -uid "07A9A40A-4FB2-4673-490A-D5BAD7C0C1A6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 100 0;
createNode animCurveTA -n "Tail_2_CNTRL_rotateX";
	rename -uid "7C1D1B36-4725-6F1C-5399-B18899102E8A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -2.6248170928500238 25 -3.5531221520056246
		 57 -2.6252931281413323 78 -3.3752312452128157 100 -2.6248170928500238;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_2_CNTRL_rotateY";
	rename -uid "4E3AB4FE-44F1-5B2B-77D6-0DB4BD8F667A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.56204403979310891 57 -0.53446146587044874
		 100 0.56204403979310891;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_2_CNTRL_rotateZ";
	rename -uid "60379D25-48AE-0EB7-6EA5-C0975622968F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.2443707798851623e-17 57 0.046847478083926537
		 100 1.2443707798851623e-17;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_3_CNTRL_rotateX";
	rename -uid "7F8EC0B4-477F-2D2A-78B2-488CCE61EB0F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.8061918352018624 50 -4.8297900271491834
		 100 -4.8061918352018624;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_3_CNTRL_rotateY";
	rename -uid "32DA0B3C-431E-9C18-AD4F-F88927789FB2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.5722439155666499 50 -5.6594994957308709
		 100 2.5722439155666499;
	setAttr -s 3 ".kit[1:2]"  18 1;
	setAttr -s 3 ".kot[1:2]"  18 1;
	setAttr -s 3 ".kix[0:2]"  0.95435532043501925 1 0.95472978602422076;
	setAttr -s 3 ".kiy[0:2]"  0.29867360505637564 0 0.2974744286121847;
	setAttr -s 3 ".kox[0:2]"  0.95435532737652884 1 0.95472979581164563;
	setAttr -s 3 ".koy[0:2]"  0.2986735828760867 0 0.29747439719991597;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_3_CNTRL_rotateZ";
	rename -uid "11E5057A-4792-435D-7305-2284E1D61CC0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.2436572466736199e-17 50 0.47741659498291161
		 100 1.2436572466736199e-17;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_4_CNTRL_rotateX";
	rename -uid "354731C8-474D-1D8E-60F9-6DB1BC021A05";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.065927759428116 50 -1.0676843917646943
		 100 -1.065927759428116;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_4_CNTRL_rotateY";
	rename -uid "D586741B-4D83-1F2A-299E-BF9E99BBC461";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.1755926830316223 50 -3.2869348844608242
		 100 2.1755926830316223;
	setAttr -s 3 ".kit[1:2]"  18 1;
	setAttr -s 3 ".kot[1:2]"  18 1;
	setAttr -s 3 ".kix[0:2]"  0.98816870005803803 1 0.98826004887204211;
	setAttr -s 3 ".kiy[0:2]"  0.15337085846277018 0 0.15278113693590883;
	setAttr -s 3 ".kox[0:2]"  0.98816869882444236 1 0.98826005272115547;
	setAttr -s 3 ".koy[0:2]"  0.15337086641082989 0 0.1527811120380369;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_4_CNTRL_rotateZ";
	rename -uid "25D76560-4864-80F8-13BD-0A8820874ED9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.66702788725003159 50 -0.60580367179490602
		 100 -0.66702788725003159;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_5_CNTRL_rotateX";
	rename -uid "BACACDCA-4530-B773-C73F-8388E5B69645";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 9.9924241375372223 50 10.009066672254868
		 100 9.9924241375372223;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_5_CNTRL_rotateY";
	rename -uid "DDBD6E88-4569-574A-0397-3ABD14005067";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.5161293860357086 50 -3.287690761729388
		 100 3.5161293860357086;
	setAttr -s 3 ".kit[1:2]"  18 1;
	setAttr -s 3 ".kot[1:2]"  18 1;
	setAttr -s 3 ".kix[0:2]"  0.95108401412534194 1 0.95142482625200164;
	setAttr -s 3 ".kiy[0:2]"  0.30893235194007529 0 0.30788114588481791;
	setAttr -s 3 ".kox[0:2]"  0.95108401171010815 1 0.95142482758946689;
	setAttr -s 3 ".koy[0:2]"  0.308932359375652 0 0.3078811417517372;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_5_CNTRL_rotateZ";
	rename -uid "42772BF3-41FE-D3AD-792B-0A95227B7DA8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 50 -0.57990686303485039 100 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_6_CNTRL_rotateX";
	rename -uid "A9196AC8-4C08-60E7-F0E6-E19CAADD8164";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 12.133882527656668 50 12.228722902990635
		 100 12.133882527656668;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_6_CNTRL_rotateY";
	rename -uid "B08E21C0-4BE9-67A2-F682-DAB9CF935F1B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.9814253649642961 50 -7.0864163661265733
		 100 1.9814253649642961;
	setAttr -s 3 ".kit[1:2]"  18 1;
	setAttr -s 3 ".kot[1:2]"  18 1;
	setAttr -s 3 ".kix[0:2]"  0.84591630152865649 1 0.84691023068827798;
	setAttr -s 3 ".kiy[0:2]"  0.53331567650696243 0 0.53173589417635503;
	setAttr -s 3 ".kox[0:2]"  0.84591626349959048 1 0.84691026489230015;
	setAttr -s 3 ".koy[0:2]"  0.53331573682659272 0 0.53173583969867422;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "Tail_6_CNTRL_rotateZ";
	rename -uid "03764CB6-4071-C59F-58F6-26A336A5F19E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.0029104756488076e-16 50 -1.5315788134192352
		 100 -1.0029104756488076e-16;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
select -ne :time1;
	setAttr ".o" 46;
	setAttr ".unw" 46;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".aoon" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 35 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 15 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :defaultTextureList1;
	setAttr -s 14 ".tx";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".mcfr" 30;
	setAttr ".ren" -type "string" "arnold";
	setAttr ".outf" 51;
	setAttr ".imfkey" -type "string" "exr";
select -ne :defaultResolution;
	setAttr ".w" 1920;
	setAttr ".h" 1080;
	setAttr ".pa" 1;
	setAttr ".dar" 1.7769999504089355;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
	setAttr ".hwfr" 30;
select -ne :defaultHideFaceDataSet;
	setAttr -s 3 ".dnsm";
select -ne :ikSystem;
connectAttr "R_Foot_Controller_R_Foot_Spread.o" "Sauropod_rig1_44RN.phl[280]";
connectAttr "R_Foot_Controller_R_Foot_Curl.o" "Sauropod_rig1_44RN.phl[281]";
connectAttr "R_Foot_Controller_R_Foot_Roll.o" "Sauropod_rig1_44RN.phl[282]";
connectAttr "R_Foot_Controller_R_Left_Toe_Curl.o" "Sauropod_rig1_44RN.phl[283]";
connectAttr "R_Foot_Controller_R_Right_Toe_Curl.o" "Sauropod_rig1_44RN.phl[284]"
		;
connectAttr "R_Foot_Controller_R_Mid_Toe_Curl.o" "Sauropod_rig1_44RN.phl[285]";
connectAttr "R_Foot_Controller_R_Left_Toe_Spread.o" "Sauropod_rig1_44RN.phl[286]"
		;
connectAttr "R_Foot_Controller_R_Right_Toe_Spread.o" "Sauropod_rig1_44RN.phl[287]"
		;
connectAttr "R_Foot_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[288]";
connectAttr "R_Foot_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[289]";
connectAttr "R_Foot_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[290]";
connectAttr "R_Foot_Controller_translateX.o" "Sauropod_rig1_44RN.phl[291]";
connectAttr "R_Foot_Controller_translateY.o" "Sauropod_rig1_44RN.phl[292]";
connectAttr "R_Foot_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[293]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[294]";
connectAttr "L_Foot_Controller_L_Foot_Spread.o" "Sauropod_rig1_44RN.phl[295]";
connectAttr "L_Foot_Controller_L_Foot_Curl.o" "Sauropod_rig1_44RN.phl[296]";
connectAttr "L_Foot_Controller_L_Foot_Roll.o" "Sauropod_rig1_44RN.phl[297]";
connectAttr "L_Foot_Controller_L_Left_Toe_Curl.o" "Sauropod_rig1_44RN.phl[298]";
connectAttr "L_Foot_Controller_L_Right_Toe_Curl.o" "Sauropod_rig1_44RN.phl[299]"
		;
connectAttr "L_Foot_Controller_L_Mid_Toe_Curl.o" "Sauropod_rig1_44RN.phl[300]";
connectAttr "L_Foot_Controller_L_Left_Toe_Spread.o" "Sauropod_rig1_44RN.phl[301]"
		;
connectAttr "L_Foot_Controller_L_Right_Toe_Spread.o" "Sauropod_rig1_44RN.phl[302]"
		;
connectAttr "L_Foot_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[303]";
connectAttr "L_Foot_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[304]";
connectAttr "L_Foot_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[305]";
connectAttr "L_Foot_Controller_translateX.o" "Sauropod_rig1_44RN.phl[306]";
connectAttr "L_Foot_Controller_translateY.o" "Sauropod_rig1_44RN.phl[307]";
connectAttr "L_Foot_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[308]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[309]";
connectAttr "Main_Body_Cog_Controller_translateX.o" "Sauropod_rig1_44RN.phl[310]"
		;
connectAttr "Main_Body_Cog_Controller_translateY.o" "Sauropod_rig1_44RN.phl[311]"
		;
connectAttr "Main_Body_Cog_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[312]"
		;
connectAttr "Main_Body_Cog_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[313]";
connectAttr "Main_Body_Cog_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[314]";
connectAttr "Main_Body_Cog_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[315]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[316]";
connectAttr "Hip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[317]";
connectAttr "Hip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[318]";
connectAttr "Hip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[319]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[320]";
connectAttr "Middle_Waist_Controller_translateX.o" "Sauropod_rig1_44RN.phl[321]"
		;
connectAttr "Middle_Waist_Controller_translateY.o" "Sauropod_rig1_44RN.phl[322]"
		;
connectAttr "Middle_Waist_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[323]"
		;
connectAttr "Middle_Waist_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[324]";
connectAttr "Middle_Waist_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[325]";
connectAttr "Middle_Waist_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[326]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[327]";
connectAttr "Upper_Body_Contraint_translateX.o" "Sauropod_rig1_44RN.phl[328]";
connectAttr "Upper_Body_Contraint_translateY.o" "Sauropod_rig1_44RN.phl[329]";
connectAttr "Upper_Body_Contraint_translateZ.o" "Sauropod_rig1_44RN.phl[330]";
connectAttr "Upper_Body_Contraint_rotateX.o" "Sauropod_rig1_44RN.phl[331]";
connectAttr "Upper_Body_Contraint_rotateY.o" "Sauropod_rig1_44RN.phl[332]";
connectAttr "Upper_Body_Contraint_rotateZ.o" "Sauropod_rig1_44RN.phl[333]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[334]";
connectAttr "Neck_Base_Contoller_translateX.o" "Sauropod_rig1_44RN.phl[335]";
connectAttr "Neck_Base_Contoller_translateY.o" "Sauropod_rig1_44RN.phl[336]";
connectAttr "Neck_Base_Contoller_translateZ.o" "Sauropod_rig1_44RN.phl[337]";
connectAttr "Neck_Base_Contoller_rotateX.o" "Sauropod_rig1_44RN.phl[338]";
connectAttr "Neck_Base_Contoller_rotateY.o" "Sauropod_rig1_44RN.phl[339]";
connectAttr "Neck_Base_Contoller_rotateZ.o" "Sauropod_rig1_44RN.phl[340]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[341]";
connectAttr "Neck_Lower_Mid_Controller_translateX.o" "Sauropod_rig1_44RN.phl[342]"
		;
connectAttr "Neck_Lower_Mid_Controller_translateY.o" "Sauropod_rig1_44RN.phl[343]"
		;
connectAttr "Neck_Lower_Mid_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[344]"
		;
connectAttr "Neck_Lower_Mid_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[345]";
connectAttr "Neck_Lower_Mid_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[346]";
connectAttr "Neck_Lower_Mid_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[347]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[348]";
connectAttr "Neck_Upper_Mid_Controller_translateX.o" "Sauropod_rig1_44RN.phl[349]"
		;
connectAttr "Neck_Upper_Mid_Controller_translateY.o" "Sauropod_rig1_44RN.phl[350]"
		;
connectAttr "Neck_Upper_Mid_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[351]"
		;
connectAttr "Neck_Upper_Mid_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[352]";
connectAttr "Neck_Upper_Mid_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[353]";
connectAttr "Neck_Upper_Mid_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[354]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[355]";
connectAttr "Head_Base_Controller_translateX.o" "Sauropod_rig1_44RN.phl[356]";
connectAttr "Head_Base_Controller_translateY.o" "Sauropod_rig1_44RN.phl[357]";
connectAttr "Head_Base_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[358]";
connectAttr "Head_Base_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[359]";
connectAttr "Head_Base_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[360]";
connectAttr "Head_Base_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[361]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[362]";
connectAttr "Head_Controller_translateX.o" "Sauropod_rig1_44RN.phl[363]";
connectAttr "Head_Controller_translateY.o" "Sauropod_rig1_44RN.phl[364]";
connectAttr "Head_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[365]";
connectAttr "Head_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[366]";
connectAttr "Head_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[367]";
connectAttr "Head_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[368]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[369]";
connectAttr "L_Upper_Top_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[370]"
		;
connectAttr "L_Upper_Top_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[371]"
		;
connectAttr "L_Upper_Top_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[372]"
		;
connectAttr "L_Upper_Top_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[373]"
		;
connectAttr "L_Upper_Top_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[374]"
		;
connectAttr "L_Upper_Top_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[375]"
		;
connectAttr "L_Upper_Top_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[376]";
connectAttr "L_Upper_Top_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[377]";
connectAttr "L_Upper_Top_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[378]";
connectAttr "L_Upper_Top_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[379]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[380]";
connectAttr "L_Upper_Mid_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[381]"
		;
connectAttr "L_Upper_Mid_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[382]"
		;
connectAttr "L_Upper_Mid_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[383]"
		;
connectAttr "L_Upper_Mid_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[384]"
		;
connectAttr "L_Upper_Mid_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[385]"
		;
connectAttr "L_Upper_Mid_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[386]"
		;
connectAttr "L_Upper_Mid_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[387]";
connectAttr "L_Upper_Mid_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[388]";
connectAttr "L_Upper_Mid_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[389]";
connectAttr "L_Upper_Mid_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[390]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[391]";
connectAttr "L_Upper_Bottom_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[392]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[393]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[394]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[395]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[396]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[397]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[398]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[399]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[400]"
		;
connectAttr "L_Upper_Bottom_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[401]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[402]";
connectAttr "R_Upper_Top_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[403]"
		;
connectAttr "R_Upper_Top_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[404]"
		;
connectAttr "R_Upper_Top_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[405]"
		;
connectAttr "R_Upper_Top_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[406]"
		;
connectAttr "R_Upper_Top_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[407]"
		;
connectAttr "R_Upper_Top_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[408]"
		;
connectAttr "R_Upper_Top_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[409]";
connectAttr "R_Upper_Top_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[410]";
connectAttr "R_Upper_Top_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[411]";
connectAttr "R_Upper_Top_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[412]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[413]";
connectAttr "R_Upper_Mid_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[414]"
		;
connectAttr "R_Upper_Mid_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[415]"
		;
connectAttr "R_Upper_Mid_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[416]"
		;
connectAttr "R_Upper_Mid_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[417]"
		;
connectAttr "R_Upper_Mid_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[418]"
		;
connectAttr "R_Upper_Mid_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[419]"
		;
connectAttr "R_Upper_Mid_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[420]";
connectAttr "R_Upper_Mid_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[421]";
connectAttr "R_Upper_Mid_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[422]";
connectAttr "R_Upper_Mid_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[423]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[424]";
connectAttr "R_Upper_Bottom_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[425]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[426]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[427]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[428]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[429]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[430]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[431]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[432]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[433]"
		;
connectAttr "R_Upper_Bottom_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[434]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[435]";
connectAttr "Lower_Jaw_Controller_Tongue_Curl_Up.o" "Sauropod_rig1_44RN.phl[436]"
		;
connectAttr "Lower_Jaw_Controller_Tongue_Curl_Tip.o" "Sauropod_rig1_44RN.phl[437]"
		;
connectAttr "Sauropod_rig1_44RN.phl[438]" "locator1_parentConstraint1.tg[0].tt";
connectAttr "Sauropod_rig1_44RN.phl[439]" "locator1_parentConstraint1.tg[0].trp"
		;
connectAttr "Sauropod_rig1_44RN.phl[440]" "locator1_parentConstraint1.tg[0].trt"
		;
connectAttr "Sauropod_rig1_44RN.phl[441]" "locator1_parentConstraint1.tg[0].tr";
connectAttr "Lower_Jaw_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[442]";
connectAttr "Lower_Jaw_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[443]";
connectAttr "Lower_Jaw_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[444]";
connectAttr "Sauropod_rig1_44RN.phl[445]" "locator1_parentConstraint1.tg[0].tro"
		;
connectAttr "Sauropod_rig1_44RN.phl[446]" "locator1_parentConstraint1.tg[0].ts";
connectAttr "Lower_Jaw_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[447]";
connectAttr "Lower_Jaw_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[448]";
connectAttr "Lower_Jaw_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[449]";
connectAttr "Sauropod_rig1_44RN.phl[450]" "locator1_parentConstraint1.tg[0].tpm"
		;
connectAttr "Lower_Jaw_Controller_visibility.o" "Sauropod_rig1_44RN.phl[451]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[452]";
connectAttr "L_Lower_Top_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[453]"
		;
connectAttr "L_Lower_Top_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[454]"
		;
connectAttr "L_Lower_Top_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[455]"
		;
connectAttr "L_Lower_Top_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[456]"
		;
connectAttr "L_Lower_Top_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[457]"
		;
connectAttr "L_Lower_Top_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[458]"
		;
connectAttr "L_Lower_Top_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[459]";
connectAttr "L_Lower_Top_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[460]";
connectAttr "L_Lower_Top_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[461]";
connectAttr "L_Lower_Top_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[462]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[463]";
connectAttr "L_Lower_Mid_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[464]"
		;
connectAttr "L_Lower_Mid_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[465]"
		;
connectAttr "L_Lower_Mid_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[466]"
		;
connectAttr "L_Lower_Mid_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[467]"
		;
connectAttr "L_Lower_Mid_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[468]"
		;
connectAttr "L_Lower_Mid_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[469]"
		;
connectAttr "L_Lower_Mid_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[470]";
connectAttr "L_Lower_Mid_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[471]";
connectAttr "L_Lower_Mid_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[472]";
connectAttr "L_Lower_Mid_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[473]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[474]";
connectAttr "L_Lower_Bottom_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[475]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[476]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[477]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[478]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[479]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[480]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[481]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[482]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[483]"
		;
connectAttr "L_Lower_Bottom_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[484]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[485]";
connectAttr "R_Lower_Bottom_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[486]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[487]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[488]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[489]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[490]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[491]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[492]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[493]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[494]"
		;
connectAttr "R_Lower_Bottom_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[495]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[496]";
connectAttr "R_Lower_Mid_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[497]"
		;
connectAttr "R_Lower_Mid_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[498]"
		;
connectAttr "R_Lower_Mid_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[499]"
		;
connectAttr "R_Lower_Mid_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[500]"
		;
connectAttr "R_Lower_Mid_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[501]"
		;
connectAttr "R_Lower_Mid_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[502]"
		;
connectAttr "R_Lower_Mid_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[503]";
connectAttr "R_Lower_Mid_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[504]";
connectAttr "R_Lower_Mid_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[505]";
connectAttr "R_Lower_Mid_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[506]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[507]";
connectAttr "R_Lower_Top_Lip_Controller_translateX.o" "Sauropod_rig1_44RN.phl[508]"
		;
connectAttr "R_Lower_Top_Lip_Controller_translateY.o" "Sauropod_rig1_44RN.phl[509]"
		;
connectAttr "R_Lower_Top_Lip_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[510]"
		;
connectAttr "R_Lower_Top_Lip_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[511]"
		;
connectAttr "R_Lower_Top_Lip_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[512]"
		;
connectAttr "R_Lower_Top_Lip_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[513]"
		;
connectAttr "R_Lower_Top_Lip_Controller_scaleX.o" "Sauropod_rig1_44RN.phl[514]";
connectAttr "R_Lower_Top_Lip_Controller_scaleY.o" "Sauropod_rig1_44RN.phl[515]";
connectAttr "R_Lower_Top_Lip_Controller_scaleZ.o" "Sauropod_rig1_44RN.phl[516]";
connectAttr "R_Lower_Top_Lip_Controller_visibility.o" "Sauropod_rig1_44RN.phl[517]"
		;
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[518]";
connectAttr "L_Eyeball_Controller_Top_Eyelid.o" "Sauropod_rig1_44RN.phl[519]";
connectAttr "L_Eyeball_Controller_Bottom_Eyelid.o" "Sauropod_rig1_44RN.phl[520]"
		;
connectAttr "L_Eyeball_Controller_translateX.o" "Sauropod_rig1_44RN.phl[521]";
connectAttr "L_Eyeball_Controller_translateY.o" "Sauropod_rig1_44RN.phl[522]";
connectAttr "L_Eyeball_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[523]";
connectAttr "L_Eyeball_Controller_visibility.o" "Sauropod_rig1_44RN.phl[524]";
connectAttr "L_Eyeball_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[525]";
connectAttr "L_Eyeball_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[526]";
connectAttr "L_Eyeball_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[527]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[528]";
connectAttr "R_Eyeball_Controller_Top_Eyelid.o" "Sauropod_rig1_44RN.phl[529]";
connectAttr "R_Eyeball_Controller_Bottom_Eyelid.o" "Sauropod_rig1_44RN.phl[530]"
		;
connectAttr "R_Eyeball_Controller_translateX.o" "Sauropod_rig1_44RN.phl[531]";
connectAttr "R_Eyeball_Controller_translateY.o" "Sauropod_rig1_44RN.phl[532]";
connectAttr "R_Eyeball_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[533]";
connectAttr "R_Eyeball_Controller_visibility.o" "Sauropod_rig1_44RN.phl[534]";
connectAttr "R_Eyeball_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[535]";
connectAttr "R_Eyeball_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[536]";
connectAttr "R_Eyeball_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[537]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[538]";
connectAttr "Tail_1_CNTRL_Tail_Curl_Side.o" "Sauropod_rig1_44RN.phl[539]";
connectAttr "Tail_1_CNTRL_Tail_Curl_Up.o" "Sauropod_rig1_44RN.phl[540]";
connectAttr "Tail_1_CNTRL_rotateX.o" "Sauropod_rig1_44RN.phl[541]";
connectAttr "Tail_1_CNTRL_rotateY.o" "Sauropod_rig1_44RN.phl[542]";
connectAttr "Tail_1_CNTRL_rotateZ.o" "Sauropod_rig1_44RN.phl[543]";
connectAttr "Tail_2_CNTRL_rotateX.o" "Sauropod_rig1_44RN.phl[544]";
connectAttr "Tail_2_CNTRL_rotateY.o" "Sauropod_rig1_44RN.phl[545]";
connectAttr "Tail_2_CNTRL_rotateZ.o" "Sauropod_rig1_44RN.phl[546]";
connectAttr "Tail_3_CNTRL_rotateX.o" "Sauropod_rig1_44RN.phl[547]";
connectAttr "Tail_3_CNTRL_rotateY.o" "Sauropod_rig1_44RN.phl[548]";
connectAttr "Tail_3_CNTRL_rotateZ.o" "Sauropod_rig1_44RN.phl[549]";
connectAttr "Tail_4_CNTRL_rotateX.o" "Sauropod_rig1_44RN.phl[550]";
connectAttr "Tail_4_CNTRL_rotateY.o" "Sauropod_rig1_44RN.phl[551]";
connectAttr "Tail_4_CNTRL_rotateZ.o" "Sauropod_rig1_44RN.phl[552]";
connectAttr "Tail_5_CNTRL_rotateX.o" "Sauropod_rig1_44RN.phl[553]";
connectAttr "Tail_5_CNTRL_rotateY.o" "Sauropod_rig1_44RN.phl[554]";
connectAttr "Tail_5_CNTRL_rotateZ.o" "Sauropod_rig1_44RN.phl[555]";
connectAttr "Tail_6_CNTRL_rotateX.o" "Sauropod_rig1_44RN.phl[556]";
connectAttr "Tail_6_CNTRL_rotateY.o" "Sauropod_rig1_44RN.phl[557]";
connectAttr "Tail_6_CNTRL_rotateZ.o" "Sauropod_rig1_44RN.phl[558]";
connectAttr "L_Shoulder_Controller_translateY.o" "Sauropod_rig1_44RN.phl[559]";
connectAttr "L_Shoulder_Controller_translateZ.o" "Sauropod_rig1_44RN.phl[560]";
connectAttr "L_Shoulder_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[561]";
connectAttr "L_Shoulder_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[562]";
connectAttr "L_Shoulder_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[563]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[564]";
connectAttr "L_Elbow_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[565]";
connectAttr "L_Elbow_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[566]";
connectAttr "L_Elbow_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[567]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[568]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[569]";
connectAttr "R_Shoulder_Contoller_translateX.o" "Sauropod_rig1_44RN.phl[570]";
connectAttr "R_Shoulder_Contoller_translateY.o" "Sauropod_rig1_44RN.phl[571]";
connectAttr "R_Shoulder_Contoller_translateZ.o" "Sauropod_rig1_44RN.phl[572]";
connectAttr "R_Shoulder_Contoller_rotateX.o" "Sauropod_rig1_44RN.phl[573]";
connectAttr "R_Shoulder_Contoller_rotateY.o" "Sauropod_rig1_44RN.phl[574]";
connectAttr "R_Shoulder_Contoller_rotateZ.o" "Sauropod_rig1_44RN.phl[575]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[576]";
connectAttr "R_Elbow_Controller_rotateX.o" "Sauropod_rig1_44RN.phl[577]";
connectAttr "R_Elbow_Controller_rotateY.o" "Sauropod_rig1_44RN.phl[578]";
connectAttr "R_Elbow_Controller_rotateZ.o" "Sauropod_rig1_44RN.phl[579]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[580]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[581]";
connectAttr "locator1_parentConstraint1.ctx" "locator1.tx";
connectAttr "locator1_parentConstraint1.cty" "locator1.ty";
connectAttr "locator1_parentConstraint1.ctz" "locator1.tz";
connectAttr "locator1_parentConstraint1.crx" "locator1.rx";
connectAttr "locator1_parentConstraint1.cry" "locator1.ry";
connectAttr "locator1_parentConstraint1.crz" "locator1.rz";
connectAttr "locator1_parentConstraint1.w0" "locator1_parentConstraint1.tg[0].tw"
		;
connectAttr "locator1.ro" "locator1_parentConstraint1.cro";
connectAttr "locator1.pim" "locator1_parentConstraint1.cpim";
connectAttr "locator1.rp" "locator1_parentConstraint1.crp";
connectAttr "locator1.rpt" "locator1_parentConstraint1.crt";
connectAttr "Floor.di" "pPlane1.do";
connectAttr "polyPlane1.out" "pPlaneShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Tail_Controller_Base_Tail_Curl_Side.o" "Sauropod_rig1_44RN.phl[38]"
		;
connectAttr "Tail_Controller_Base_Tail_Curl_Up.o" "Sauropod_rig1_44RN.phl[39]";
connectAttr "Tail_Controller_Base_rotateX.o" "Sauropod_rig1_44RN.phl[40]";
connectAttr "Tail_Controller_Base_rotateY.o" "Sauropod_rig1_44RN.phl[41]";
connectAttr "Tail_Controller_Base_rotateZ.o" "Sauropod_rig1_44RN.phl[42]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[43]";
connectAttr "Tail_Controller_LowerMid_rotateX.o" "Sauropod_rig1_44RN.phl[44]";
connectAttr "Tail_Controller_LowerMid_rotateY.o" "Sauropod_rig1_44RN.phl[45]";
connectAttr "Tail_Controller_LowerMid_rotateZ.o" "Sauropod_rig1_44RN.phl[46]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[47]";
connectAttr "Tail_Controller_Mid_1_rotateX.o" "Sauropod_rig1_44RN.phl[48]";
connectAttr "Tail_Controller_Mid_1_rotateY.o" "Sauropod_rig1_44RN.phl[49]";
connectAttr "Tail_Controller_Mid_1_rotateZ.o" "Sauropod_rig1_44RN.phl[50]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[51]";
connectAttr "Tail_Controller_Mid_2_rotateX.o" "Sauropod_rig1_44RN.phl[52]";
connectAttr "Tail_Controller_Mid_2_rotateY.o" "Sauropod_rig1_44RN.phl[53]";
connectAttr "Tail_Controller_Mid_2_rotateZ.o" "Sauropod_rig1_44RN.phl[54]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[55]";
connectAttr "Tail_Controller_Upper_Mid_rotateX.o" "Sauropod_rig1_44RN.phl[56]";
connectAttr "Tail_Controller_Upper_Mid_rotateY.o" "Sauropod_rig1_44RN.phl[57]";
connectAttr "Tail_Controller_Upper_Mid_rotateZ.o" "Sauropod_rig1_44RN.phl[58]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[59]";
connectAttr "Tail_Controller_Tip_rotateX.o" "Sauropod_rig1_44RN.phl[60]";
connectAttr "Tail_Controller_Tip_rotateY.o" "Sauropod_rig1_44RN.phl[61]";
connectAttr "Tail_Controller_Tip_rotateZ.o" "Sauropod_rig1_44RN.phl[62]";
connectAttr "Controls.di" "Sauropod_rig1_44RN.phl[63]";
connectAttr "L_Eyeball_Controller_Both_Eyelids.o" "Sauropod_rig1_44RN.phl[268]";
connectAttr "R_Eyeball_Controller_Both_Eyelids.o" "Sauropod_rig1_44RN.phl[279]";
connectAttr "sharedReferenceNode.sr" "Sauropod_rig1_44RN.sr";
connectAttr "layerManager.dli[1]" "Controls.id";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "Ground.oc" "phong1SG.ss";
connectAttr "pPlaneShape1.iog" "phong1SG.dsm" -na;
connectAttr "phong1SG.msg" "materialInfo1.sg";
connectAttr "Ground.msg" "materialInfo1.m";
connectAttr "phong1SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "Ground.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "layerManager.dli[3]" "Floor.id";
connectAttr "phong1SG.pa" ":renderPartition.st" -na;
connectAttr "Ground.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of sauropodomorph_chew04.ma
